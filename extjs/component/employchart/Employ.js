/**
* Модель данных для графиков приема.
* 
* Описывает такие поля:
* 	date - дата приема
*	sessions - массив пиемов на эту дату
*/
Ext.define('MED.EMKU.organize.component.employchart.Employ',{
	extend: 'Ext.data.Model',	
	alias: ['model.employchartmodel'],
	fields: [		
		
		{name: 'date', type: 'date'},


		{name: 'sessions', type: 'auto', defaultValue:[] }


	]

});