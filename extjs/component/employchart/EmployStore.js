/**
* Временный стор для работы с графиком.
*
* Используется для хранения и получения накладываемых на график данных
* 
*
* @autor Yeromenko V.A.
*/
Ext.define('MED.EMKU.organize.component.employchart.EmployStore', {
	
	extend: 'Ext.data.Store',

	requires: [
		'MED.EMKU.organize.component.employchart.Employ'
	],

	model:'MED.EMKU.organize.component.employchart.Employ',

	alias: ['store.employchartstore'],

	proxy: {
		type: 'memory',
		reader: 'json'
	},

	/**
	* Стор с сохраненными приемами
	* @property {} 
	*/
	sesStore: null,

	/**
	* Стор с накладываемыми данными
	*
	* @property {} 
	*/
	overlayStore: null,

	/**
	* Получить список приемов всех на указанныю дату
	* Возвращаются как реальные приемы, так и накладываемые
	* @param {Date} date
	* @return {Ext.util.MixedCollection} коллекция приемов
	*/
	getSessions: function(date) {
		var dataSessions = this.getDataSessions(date); //реальные приемы
		var overlaySessions = this.getOverlaySessions(date); //накладываемые приемы

		//Объединение коллекций в одну 
		overlaySessions.each(function(item, key){
			dataSessions.add(key, item);
		});

		return dataSessions;
	},

	/**
	* Получить список накладываемых приемов на указанныю дату
	*
	* @param {Date} date
	* @return {Ext.util.MixedCollection} коллекция приемов
	*/
	getOverlaySessions: function(date) {
		var store = this.overlayStore,
			d1 = Ext.Date.clearTime(date, true),//Сбрасываем время до 00:00
			d2 = Ext.Date.clone(d1);

		//устанавливаем верхний придел времени (24:00)
		d2.setHours(23,59,59,99);

		// в коллекцию попадают записи у которых time_begin попадает 
		// в интервал времени [d1:d2]
		var collection = store.queryBy(function(model) {
			return Ext.Date.between(model.get('time_begin'), d1, d2 );
		}, this);
	
		return collection;

	},

	/**
	* Получить список реальных приемов на указанныю дату
	*
	* @param {Date} date
	* @return {Ext.util.MixedCollection} коллекция приемов
	*/
	getDataSessions: function(date) {
		var store = this.sesStore,
			d1 = Ext.Date.clearTime(date, true), //Сбрасываем время до 00:00
			d2 = Ext.Date.clone(d1),
			collection;

		//устанавливаем верхний придел времени (24:00)
		d2.setHours(23,59,59,99);

		// в коллекцию попадают записи у которых time_begin попадает 
		// в интервал времени [d1:d2]
		collection = store.queryBy(function(model) {
			return Ext.Date.between(model.get('time_begin'), d1, d2 );
		}, this);

		return collection;
	},




});