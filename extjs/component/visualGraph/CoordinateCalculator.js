Ext.define('MED.EMKU.organize.component.visualGraph.CoordinateCalculator', {
	extend: 'Ext.Base',

	//Параметры
	params: {

		//Масштаб
		scale_x: 1,
		scale_y: 50,

		//Количество часов
		count_hours: 24,

		//Часа начала
		start_hour: 0,

		//Отступ слева
		start_x: 50,

		//Отсутп сверху
		start_y: 50,

		//Дата с которой начинается график
		start_date: null,

		//Количество дат
		count_dates: 0,

		//Минимальная высота
		min_height: 50,

		window_height: 0,

		window_width: 0,

		dates_array: []
	},


	/**
	 * Установка параметров
	 * @param  object params параметры перечислены выше
	 * @return boolean
	 */
	setParams: function(params){
		var me = this;

		if (typeof(params)==='object' && params!=null){
			Ext.apply(me.params, params);
		}

		return true;
	},

	/**
	 * Возвращает набор параметров
	 * @return object
	 */
	getParams: function(){
		return this.params;
	},


	/**
	 * Просчет координаты x
	 * @param  date time Время приема
	 * @return float - х координата
	 */
	getX: function(time){
		var me = this,
			start_time = Ext.Date.parse((me.params.start_hour<10 ? '0' : '')+me.params.start_hour+':00:00', 'H:i:s'),
			converted_time = Ext.Date.parse(Ext.Date.format(time, 'H:i:s'), 'H:i:s'),
			x = (((converted_time.getTime()-start_time.getTime())/1000/60))*me.params.scale_x+ me.params.start_x;
		return (x);
	},

	
	/**
	 * Просчет координаты y
	 * @param  date current_date  дата приема
	 * @return float - y координата
	 */
	getY: function(current_date){
		var me = this,
			height = me.getHeight(),
			//Какой по счету день относительно даты начала
			day_num = parseInt(Ext.Date.format(current_date, 'd')) - parseInt(Ext.Date.format(me.params.start_date, 'd')),
			y = (height*day_num);//me.params.start_y + 

		return (y);
	},


	/**
	 * Просчет ширины блока
	 * @param  date start_time Время начала приема
	 * @param  date end_time   Время окончания приема
	 * @return float - ширину блока
	 */
	getWidth: function(start_time, end_time){
		var me = this,
			width = ((end_time.getTime()-start_time.getTime())/1000/60)*me.params.scale_x;


		//Если это час между 23 и 24
		if (Ext.Date.format(end_time, 'H:i:s')==='00:00:00' && Ext.Date.format(start_time, 'H:i:s')==='23:00:00'){
			width = 60*me.params.scale_x;
		}

		return (width);
	},


	/**
	 * Подсчет высоты блока
	 * @return float - высота блока
	 */
	getHeight: function(){
		var me = this;

		//!!!!!!!!!!!!
		if (me.params.scale_y<me.params.min_height){
			return me.params.min_height;
		}

		return me.params.scale_y;
	},

	/**
	 * Подсчет номера дня
	 * @param date day день
	 * @return void
	 */	
	getDayNum: function(day){
		var me = this,
			params = me.params,
			day_num = 0,
			val = null;

		for (var key in params.dates_array){
			val = params.dates_array[key];

			if (Ext.Date.format(day, 'Y-m-d')===Ext.Date.format(val, 'Y-m-d')){
				return day_num;
			}

			day_num++;
		}

		return 0;
	},
});
