Ext.define('MED.EMKU.organize.component.visualGraph.SideBlock', {
	extend: 'Ext.Component',

	/*Параметры*/

	//Селектор
	selector: null,

	//Ссылка на html объект
	link: null,

	//Координата y
	top: 0,

	//Координата x
	left: 0,

	//Ширина
	width: 0,

	//Высота
	height: 0,

	//Содержимое блока
	text: '',

	//Всплывающая подсказка
	tooltip: '',

	//Дополнительные классы
	classes: '',

	//Дата приема
	date: null,

	//Компонент подсчета координат
	coordCalc: null,

	//Компонент содержимого
	content: null,

	z_index: 2,


	initComponent: function(param){
		var me = this;

		me.selector = 'side-block-'+me.getId();

		Ext.apply(me, param);

	},


	getParams: function(){
		return this;
	},


	draw: function(){
		var me = this;

		me.recalc();

		var block = '<div class="side_block '+me.selector
					+' '+me.classes+'" '+
					(me.tooltip!='' ? 'title="'+me.tooltip+'"': '')+
					' style="width: '+me.width+'px; height: '+me.height+'px; top: '+
					me.top+'px; left: '+me.left+'px; z-index: '+me.z_index+';"><table><tr><td>'+me.text+'</td></tr></table></div>';

		me.content.addBlock(block);
	},


	//Перерисовка компонента
	redraw: function(){
		var me = this;

		me.recalc();
		if (me.link===null){
			me.link = Ext.select('.'+me.selector);
		}
		me.link.setHeight(me.height);
		me.link.setTop(me.top);
		
	},

	//Пересчет параметров блока
	recalc: function(){
		var me = this;
		me.top = me.coordCalc.getY(me.date);
		me.height = me.coordCalc.getHeight();
	},
});