Ext.define('MED.EMKU.organize.component.visualGraph.WebBlock', {
	extend: 'Ext.Component',

	/*Параметры*/

	//Селектор
	selector: null,

	//Ссылка на html объект
	link: null,

	//Координата y
	top: 0,

	//Координата x
	left: 0,

	//Ширина
	width: 0,

	//Высота
	height: 0,

	//Содержимое блока
	text: '',

	//Всплывающая подсказка
	tooltip: '',

	//Главный класс
	main_class: 'web_block',

	//Дополнительные классы
	classes: '',


	//Направление сетки
	direction: 'vertical',

	time_start:null,
	time_end: null,
	date_priem: null,


	//Компонент подсчета координат
	coordCalc: null,

	//Компонент содержимого
	content: null,

	z_index: 1,


	initComponent: function(param){
		var me = this;

		me.selector = 'web-element-'+me.getId();

		Ext.apply(me, param);

	},


	getParams: function(){
		return this;
	},


	draw: function(){
		var me = this;

		me.recalc();

		var block = '<div class="' + me.main_class+ ' ' +me.selector
					+' '+me.classes+'" '+
					(me.tooltip!='' ? 'title="'+me.tooltip+'"': '')+
					' style="width: '+me.width+'px; height: '+me.height+'px; top: '+
					me.top+'px; left: '+me.left+'px; z-index: '+me.z_index+';">'+me.text+'</div>';

		me.content.addBlock(block);
	},


	//Перерисовка компонента
	redraw: function(){
		var me = this;

		me.recalc();

		if (me.link===null){
			me.link = Ext.select('.'+me.selector);
		}


		if (me.direction=='vertical'){
			me.link.setLeft(me.left);
			me.link.setWidth(me.width);
			me.link.setHeight(me.height);			
		}


		if (me.direction=='horizontal'){
			me.link.setTop(me.top);
			me.link.setWidth(me.width);
			me.link.setHeight(me.height);
			me.link.setLeft(me.left);
		}
		
	},

	//Пересчет параметров блока
	recalc: function(){
		var me = this,
			coordParams = me.coordCalc.getParams();

		if (me.direction=='vertical'){
			me.left = me.coordCalc.getX(me.time_start);
			me.width = me.coordCalc.getWidth(me.time_start, me.time_end);
			me.height = coordParams.window_height-coordParams.start_y;			
		}

		if (me.direction=='horizontal'){
			me.top = me.coordCalc.getY(me.date_priem);
			me.width = coordParams.window_width-coordParams.start_x;
			me.height = me.coordCalc.getHeight();
			me.left = coordParams.start_x;
		}

	},
});