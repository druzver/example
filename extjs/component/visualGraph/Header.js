/*
	Компонент отрисовки шапки графика


*/
Ext.define('MED.EMKU.organize.component.visualGraph.Header', {
	extend: 'Ext.Component',

	/*Параметры*/
	params: {
		//Отступ слева
		start_x: 50,

		//Текст первой ячейки
		first_column_text: '',

		//Минимальная ширина одного блока
		min_width: 40, 

		//Высота
		height: 50,

		//Масштаб
		scale_x: 1,

		//Количество часов отображаемых в шапке
		count_hours: 24,

		//Час с которого начинается нумерация
		start_hour: 0,


		z_index: 2,
	},

	
	//Результат
	result: '',


	//Компонент расчета координат
	coordCalc: null, 


	//пересчитанная ширина блока
	current_width: 50,

	initComponent:function(param){
		var me = this;

		me.callParent(param);
	},


	//Метод для задания параметров
	setParams: function(params){
		var me = this;

		if (typeof(params)==='object' && params!=null){
			Ext.apply(me.params, params);
		}

		return true;
	},


	//Очистка результата
	clearResult: function(){
		this.result = '';
	},


	//Открытие враппера блока
	startWrapper: function(){
		this.result += '<div class="header_container">'+
						'<div class="first_block x-column-header" style="width: '+this.params.start_x+'px; height: '+this.params.height+'px;"><table><tr><td>'+this.params.first_column_text+'</td></tr></table></div>';
	},


	//Закрытие враппера
	endWrapper: function(){
		this.result += '</div>';
	},


	//Добавление верхнего блока
	addBlock: function(text){
		this.result += '<div class="header_block x-column-header" style="width: '+this.current_width+'px; height: '+this.params.height+'px;">'+text+'</div>';
	},


	//Получение результата
	getResult: function(){
		return this.result;
	},


	getMinWidth: function(){
		return this.params.min_width;
	},


	//Задание текущей ширины блока
	setCurrentWidth: function(){
		var me = this,
			
			//Текущий час
			current_time = Ext.Date.parse(((me.params.start_hour<10 ? '0' : '')+me.params.start_hour+':00:00'), 'H:i:s'),
			
			//След. час
			next_time = Ext.Date.parse(((me.params.start_hour<9? '0' : '')+(me.params.start_hour+1)+':00:00'), 'H:i:s'),
		
			//Получение ширины блока
			width = me.coordCalc.getWidth(current_time, next_time);


		this.current_width = this.params.min_width;

		if (width>=this.params.min_width){
			this.current_width = width;
		}
	},


	//Получение текущей ширины
	getCurrentWidth: function(){
		return this.current_width;
	},


	//Метод отрисовки
	draw: function(){
		var me = this;

		me.clearResult();

		//Установка ширины блока
		me.setCurrentWidth();


		if (me.params.start_hour>=23){
			return;
		}


		//Открытие враппера
		me.startWrapper();


		//Отрисовка блоков в цикле
		for (var i = me.params.start_hour; i <= me.params.count_hours; i++) {
			me.addBlock((i<10 ? '0' : '')+i+':00');
		};


		me.endWrapper();


		return me.getResult();
	},


	//Метод перерисовки
	redraw: function(){
		var me = this,
			blocks = Ext.select('.header_block');

		me.setCurrentWidth();

		blocks.setWidth(me.getCurrentWidth());
	},


});