Ext.define('MED.EMKU.organize.component.visualGraph.Block', {
	extend: 'Ext.Component',

	/*Параметры*/

	//Селектор
	selector: null,

	//Ссылка на html объект
	link: null,

	//Координата y
	top: 0,

	//Координата x
	left: 0,

	//Ширина
	width: 0,

	//Высота
	height: 0,

	//Содержимое блока
	text: '',

	//Всплывающая подсказка
	tooltip: '',

	//Дополнительные классы
	classes: '',

	//Дата приема
	date_priem: null,

	//Время начала
	time_start: null,

	//Время окончания
	time_end: null,


	//Компонент подсчета координат
	coordCalc: null,

	//Компонент содержимого
	content: null,

	z_index: 2,


	initComponent: function(param){
		var me = this;

		me.selector = 'priem-block-'+me.getId();

		Ext.apply(me, param);

	},


	getParams: function(){
		return this;
	},


	/**
	 * Функция отрисовки
	 * @return void
	 */
	draw: function(){
		var me = this;

		me.recalc();

		var block = '<div class="time_block '+me.selector
					+' '+me.classes+'" '+
					(me.tooltip!='' ? 'title="'+me.tooltip+'"': '')+
					' style="width: '+me.width+'px; height: '+me.height+'px; top: '+
					me.top+'px; left: '+me.left+'px; z-index: '+me.z_index+';">'+me.text+'</div>';

		me.content.addBlock(block);
	},


	/**
	 * Перерисовка компонента
	 * @return void
	 */
	redraw: function(){
		var me = this;

		me.recalc();

		if (me.link===null){
			me.link = Ext.select('.'+me.selector);
		}

		me.link.setWidth(me.width);
		me.link.setHeight(me.height);
		me.link.setTop(me.top);
		me.link.setLeft(me.left);
		
	},

	/**
	 * Пересчет параметров блока
	 * @return void
	 */
	recalc: function(){
		var me = this;

		me.left = me.coordCalc.getX(me.time_start);
		me.top = me.coordCalc.getY(me.date_priem);
		me.width = me.coordCalc.getWidth(me.time_start, me.time_end);
		me.height = me.coordCalc.getHeight();
	},
});