/*

	Компонент содержимого (блоков времени и приемов)
*/
Ext.define('MED.EMKU.organize.component.visualGraph.Content',{
	extend: 'Ext.Component',

	//Параметры
	// params: {
		//Блоки приемов
		time_blocks: [],


		//Блоки времени
		side_block: [],


		vertical_web: [],


		horizontal_web: [],


		current_timeline: null,

		current_time_start: null,

		current_time_end: null,

		//Отступ слева
		start_x : 50,

		dates_array: [],

		active_time_line_class: '',
	// },

	store: null,

	coordCalc: null,

	result: '',

	//Пересекается ли текущее время с приемами
	crossed_time_line : false,


	initComponent: function(param){
		var me = this;

		Ext.apply(me, param);

	},



	//Задание параметров
	setParams: function(params){
		var me = this;

		Ext.apply(me, params);
	},

	/**
	 * Функция отрисовки
	 * @return string html
	 */
	draw: function(){
		var me = this;

		me.cleanResult();

		me.startWrapper();

		me.drawSideBlocks();

		me.drawPriemBlocks();

		me.drawWeb();

		me.endWrapper();

		return me.getResult();
	},


	/**
	 * Перерисовка компонента
	 * @return void
	 */
	redraw: function(){
		var me = this;

		me.redrawPriemBlocks();

		me.redrawSideBlocks();

		me.redrawWeb();
	},


	/**
	 * Отрисовка боковых блоков
	 * @return void
	 */
	drawSideBlocks: function(){
		var me = this,
			params = me.coordCalc.getParams(),
			cur_date = null,
			side_block = null,
			val = null;

		if (params.start_date!=null && params.count_dates!=null){
			
			cur_date = Ext.clone(params.start_date);

			for (var i = 1; i <= params.count_dates; i++) {
				
				side_block = Ext.create('MED.EMKU.organize.component.visualGraph.SideBlock',{
					coordCalc: me.coordCalc,
					content: me,
					date: Ext.clone(cur_date),
					text: Ext.Date.format(cur_date, 'd.m.Y'),
					width: params.start_x,
					classes: 'x-column-header'
				});

				side_block.draw();

				me.side_block.push(side_block);
				cur_date.setDate(cur_date.getDate()+1);
			};

		}

	},

	/**
	 * Перерисовка боковых блоков
	 * @return void
	 */
	redrawSideBlocks: function(){
		var me = this,
			params = me;


		if (params.side_block!=null && params.side_block.length>0){
			
			var val = null,
				key = null;

			for (key in params.side_block){
				val = params.side_block[key];

				val.redraw();
			}

		}
	},


	/**
	 * Функция открытия враппера
	 * @return void
	 */
	startWrapper: function(){
		var me = this;

		me.result += '<div class="organize_wrap">';
	},


	/**
	 * Функция закрытия враппера
	 * @return void
	 */
	endWrapper: function(){
		var me = this;

		me.result += '</div>';
	},


	/**
	 * Функция очистки результата
	 * @return void
	 */
	cleanResult: function(){
		var me = this;

		me.result = '';
	},


	/**
	 * Функция добавления блока в результат
	 * @param  string block html код блока
	 * @return void
	 */
	addBlock: function(block){
		var me = this;

		me.result += block;
	},


	/**
	 * Функция получения результата
	 * @return string html
	 */
	getResult: function(){
		return this.result;
	},


	/**
	 * Отрисовка блоков приемов
	 * @return void
	 */
	drawPriemBlocks: function(){
		var me = this;

		if (me.store!==null && me.coordCalc!==null){
			var new_block = null;

			me.crossed_time_line = false;
			me.store.each(function(record){
				//Создание нового блока
				new_block = Ext.create('MED.EMKU.organize.component.visualGraph.Block',{
					coordCalc: me.coordCalc,
					content: me,
					date_priem: record.get('date'),
					time_start: record.get('start_time'),
					time_end: record.get('end_time'),
					classes: (me.inCurrentTime(record.get('start_time'), record.get('end_time')) ? 'crossed_time' : '')
				});

				new_block.draw();

				me.time_blocks.push(new_block);
			});

		}
	},

	/**
	 * Перерисовка блоков приемов
	 * @return void
	 */
	redrawPriemBlocks: function(){
		var me = this,
			params = me;


		if (params.time_blocks!=null && params.time_blocks.length>0){
			
			var val = null,
				key = null;

			for (key in params.time_blocks){
				val = params.time_blocks[key];

				val.redraw();
			}

		}
	},


	/**
	 * Отрисовка сетки
	 * @return void
	 */
	drawWeb: function(){
		var me = this;

		me.drawVerticalWeb();

		me.drawHorizontalWeb();

		me.drawCurrentTimeLine();
	},


	/**
	 * Отрисовка вертикальной сетки
	 * @return void
	 */
	drawVerticalWeb: function(){
		var me = this,
			params = me.coordCalc.getParams(),
			count_hours = params.count_hours,
			start = Ext.clone(params.start_hour),
			current_block = null;

		for (var i = start; i <count_hours; i++) {
			current_block = Ext.create('MED.EMKU.organize.component.visualGraph.WebBlock', {
				direction: 'vertical',
				time_start: Ext.Date.parse((i<10 ? '0' : '')+i+':00:00', 'H:i:s'),
				time_end: (i==23 ? Ext.Date.parse('00:00:00', 'H:i:s') : Ext.Date.parse((i<9 ? '0' : '')+(i+1)+':00:00', 'H:i:s')),
				coordCalc: me.coordCalc,
				content: me
			});

			current_block.draw();

			me.vertical_web.push(current_block);
		};


	},

	/**
	 * Функция отрисовки горизонтальной сетки
	 * @return void
	 */
	drawHorizontalWeb: function(){
		var me = this,
			params = me.coordCalc.getParams(),
			current_date = Ext.clone(params.start_date),
			dates_count = Ext.clone(params.count_dates)+1,
			current_block = null,
			val = null;

		for (var i = 1; i < dates_count; i++) {
			current_block = Ext.create('MED.EMKU.organize.component.visualGraph.WebBlock', {
				direction: 'horizontal',
				date_priem: Ext.clone(current_date),
				coordCalc: me.coordCalc,
				content: me,
				classes: (i % 2 == 0 ? 'web_style' : ''),
				z_index: 0
			});

			current_block.draw();

			me.horizontal_web.push(current_block);

			current_date.setDate(current_date.getDate()+1);
		};
	},


	/**
	 * Функция перерисовки горизонтальной сетки
	 * @return void
	 */
	redrawWeb: function(){
		var me = this;

		me.redrawVerticalWeb();

		me.redrawHorizontalWeb();

		me.redrawCurrentTimeLine();
	},

	/**
	 * Функция перерисовки вертикальной сетки
	 * @return void
	 */
	redrawVerticalWeb: function(){
		var me = this,
			params = me;

		if (params.vertical_web!=null && params.vertical_web.length>0){
			
			var val = null,
				key = null;

			for (key in params.vertical_web){
				val = params.vertical_web[key];

				val.redraw();
			}

		}
	},

	/**
	 * Функция перерисовки горизонтальной сетки
	 * @return void
	 */
	redrawHorizontalWeb: function(){
		var me = this,
			params = me;

		if (params.horizontal_web!=null && params.horizontal_web.length>0){
			
			var val = null,
				key = null;

			for (key in params.horizontal_web){
				val = params.horizontal_web[key];

				val.redraw();
			}

		}
	},

	/**
	 * Функция отрисовки текущей полосы времени
	 * @return void
	 */
	drawCurrentTimeLine: function(){
		var me = this,
			params = me.coordCalc.getParams();

		me.current_timeline = Ext.create('MED.EMKU.organize.component.visualGraph.WebBlock', {
			direction: 'vertical',
			time_start: me.current_time_start,
			time_end: me.current_time_end,
			coordCalc: me.coordCalc,
			content: me,
			tooltip: (Ext.Date.format(me.current_time_start, 'H:i')+'-'+Ext.Date.format(me.current_time_end, 'H:i')),
			z_index: 3,
			main_class: 'current_border',
			classes: (me.crossed_time_line==true ? me.active_time_line_class : '')
		});

		me.current_timeline.draw();
	},


	/**
	 * перерисовка текущего времени
	 * @return void
	 */
	redrawCurrentTimeLine: function(){
		var me = this;

		me.current_timeline.redraw();
	},


	/**
	 * Проверка вхождения блока в текущее время
	 * @param  date block_start время начала
	 * @param  date block_end   время конца
	 * @return boolean
	 */
	inCurrentTime: function(block_start, block_end){
		var me = this,
			current_time_start = Ext.Date.parse(Ext.Date.format(me.current_time_start, 'H:i:s'), 'H:i:s'),
			current_time_end = Ext.Date.parse(Ext.Date.format(me.current_time_end, 'H:i:s'), 'H:i:s'),
			current_block_start = Ext.Date.parse(Ext.Date.format(block_start, 'H:i:s'), 'H:i:s'),
			current_block_end = Ext.Date.parse(Ext.Date.format(block_end, 'H:i:s'), 'H:i:s');

		if ((current_time_start<current_block_start && current_time_end>current_block_start) || 
			(current_time_start<current_block_end && current_time_end>current_block_end) || 
			(current_block_start<current_time_start && current_block_end>current_time_end)){
			me.crossed_time_line = true;
			return true;
		}

		return false;
	},

});