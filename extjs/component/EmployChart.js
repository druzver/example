/**
 * @class MED.EMKU.organize.component.EmployChart
 * @extend Ext.panel.Panel
 *
 * Компонент работающий с услугами приема 
 * 
 *
 * @author Еременко В.А
 */

Ext.define('MED.EMKU.organize.component.EmployChart', {

	extend: 'Ext.panel.Panel',

	requires: [
		'MED.EMKU.organize.component.employchart.Employ',
		'MED.EMKU.organize.component.employchart.EmployStore',

		'MED.EMKU.organize.model.Session'
	],

	alias : 'widget.employchart',

	/*
	* Стор с реальными приемами
	*
	*/
	store:null,

	/**
	* Идентификатор подразделения
	*/
	departmentId:null,

	/**
	* Дата начала
	* @property {Date}
	*/
	dateBegin: null, 

	/**
	* Дата окончания
	* @property {Date}
	*/
	dateEnd: null, 

	/**
	* Идентификатор кабинета, для которого составляется график
	* @property {Number}
	*/
	cabinetId:null,

	/**
	* идентификатор врача
	* @property {Number}
	*/
	personId:null,

	/**
	* Прокручивать график на время указанное в {#scrollToHour}
	* @property {boolean}
	*/
	scrollToCenter: true,

	/**
	* Время на которое происходит прокручивание графика
	* Прокрутка графика происходит только если {scrollToCenter} = true
	* Значения поля должны быть в пределе от 0 до 24
	*
	* если указать 7.5, то это значит прокрутить на 7часов 30 минут.
	* 
	* @property {number}
	*/
	scrollToHour: 7,

	//
	initText: 'Инициализация графика',
	toolTipDateText: 'Дата приема:',
	fieldYearText: 'Год',
	colDateText: 'Дата',
	colTimeText: 'Время',
	helpTipGreen: 'Создаваемый/Редактируемый прием',
	helpTipBlue: 'Существующий прием',
	helpTipRed: 'Ошибочное пересечение приемов',
	helpHelp: 'Помощь',
	//

	dataStoreBeforeLoad: function(){
			this.setLoading(this.initText);
	},


	dataStoreAfterLoad: function() {		
		var store = this._tmpStore,			
			sessions,
			date,
			vcolumn,
			scrollToHour = this.scrollToHour;

		//записывает в поле sessions приемы проводимые на указанную дату.
		// данные пересчитываются 1 раз после обновления стора и тем самым  (наверное) увеличивается скорость :)
		store.each(function(rec, idx, cnt){
			date = rec.get('date');
			sessions = store.getSessions( date );
			rec.set('sessions',sessions);	
		},this);
		
		//если график показывается первый раз, то прокручиваем его на середину 
		// 12 часов дня
		//
		if(this.scrollToCenter ) {
			vcolumn = this.down('grid').getView().getViewForColumn(1);
			vcolumn.scrollBy(60 * scrollToHour);
			this.scrollToCenter = false;
		}

		this.setLoading(false);
	},

	/**
	* Возвращает дату начала выборки
	* @return {Date}
	*/
	getStartDate: function(){
		var d1 =  this.dateBegin || DOCI.dateByServer();
		d1 = Ext.Date.getFirstDateOfMonth(d1);
		d1 = Ext.Date.clearTime(d1,true);
		return d1;
	},

	/**
	* Возвращает дату окончания выборки
	* @return {Date}
	*/
	getEndDate:function(){
		var d1 = this.getStartDate();
		var d2 = Ext.Date.getLastDateOfMonth(d1);
		d2.setHours(23,59,59);
		return d2;
	},


	afterRender: function() {	
		this.callParent(arguments);
		
		var grid = this.down('grid'),
			gridEl = grid.el,
			d1, d2;

		

		d1 = this.getStartDate();
		d2 = this.getEndDate();

		this.initToolTip(gridEl, this);
		gridEl.on('click',this.onGridClick,this);

		//установить диапазоны дат		
		this.setDateRange(d1,d2);

	},

	tooltipDelegate: '.session',
	tooltipCls: 'session-tip',

	/**
	* инициализация тултипа
	* @param {Ext.dom.Element}
	*/
	initToolTip:function(el) {

		this.toolTip = Ext.create('Ext.tip.ToolTip', {
			target: el,			
			delegate: this.tooltipDelegate,
			cls: this.tooltipCls,			
			trackMouse: false,			
			renderTo: Ext.getBody(),
			listeners: {				
				beforeshow: function updateTipBody(tip) {
					var content = this.sessionTipHTML(tip,tip.triggerElement);
					tip.update(content);
				},
				scope:this
			}
		});

	},

	/**
	* Шаблон всплывающего окна с информацией о приеме
	*/
	tplSessionTooltip:[		
		'<table class="x-session-tooltip" >',

			//id
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param" colspan="2">',
					'<span class="id" >№{id}</id>',
				'</td>',
				
			'</tr>',


			//Дата приема
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Дата приема:</td>',
				'<td class="x-session-tooltip-value">{time_begin:date("d F, Y")}</td>',
			'</tr>',

			//Время приема
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Время приема:</td>',
				'<td class="x-session-tooltip-value">{time_begin:date("H:i")} - {time_end:date("H:i")} </td>',
			'</tr>',

			//Тип приема
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Тип приема: </td>',
				'<td class="x-session-tooltip-value">{type_session_name}</td>',
			'</tr>',

			//Услуги
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Услуги:</td>',
				'<td class="x-session-tooltip-value">',
					'<tpl if="services">',
						'<tpl for="services">',							
							'<div>{name}</div>',							
						'</tpl>',
					'<tpl else>',
						'<div>Нет</div>',
					'</tpl>',
				'</td>',
			'</tr>',


		'</table>'
	],

	/**
	* Шаблон всплывающего окна с пересекающимся временем
	*/
	tplSessionTooltipOver:[		
		'<table class="x-session-tooltip" >',

			//id
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param" colspan="2">',
					'<span class="id" >№{id}</id>',
				'</td>',
				
			'</tr>',


			//Дата приема
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Дата приема:</td>',
				'<td class="x-session-tooltip-value">{time_begin:date("d F, Y")}</td>',
			'</tr>',

			//Время приема
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Время приема:</td>',
				'<td class="x-session-tooltip-value">{time_begin:date("H:i")} - {time_end:date("H:i")} </td>',
			'</tr>',

			//Время пересечения
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Время пересечения:</td>',
				'<td class="x-session-tooltip-value" style="color:red; font-weight: bold">{timeover}</td>',
			'</tr>',

			//Тип приема
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Тип приема: </td>',
				'<td class="x-session-tooltip-value">{type_session_name}</td>',
			'</tr>',

			//Услуги
			'<tr  class="x-session-tooltip-row">',
				'<td class="x-session-tooltip-param">Услуги:</td>',
				'<td class="x-session-tooltip-value">',
					'<tpl if="services">',
						'<tpl for="services">',							
							'<div>{name}</div>',							
						'</tpl>',
					'<tpl else>',
						'<div>Нет</div>',
					'</tpl>',
				'</td>',
			'</tr>',


		'</table>'
	],


	type_session_names:{
		'1':'Индивидуальный произвольный',
		'2':'Индивидуальный сеансовый',
		'3':'Массовый сеансовый',
	},

	//запись редактируемой сесии
	session:null,

	sessionTipHTML:function(tip,el) {
	//	console.log('tiptext ',arguments);
		var id = parseInt(el.getAttribute('data-id'),10);
		var rec = this.store.getById(id);
		var overlayStore = this.getOverlayStore();
		var data ={};
		var tpl = this.getTpl('tplSessionTooltip');

		/*
		* Поиск єлемента пересечения 
		*/
		if (Ext.get(el).dom.children[0]){
			var overSess = Ext.get(el).dom.children[0];
			var cls = overSess.getAttribute('class');
			if (cls == 'session-overlap') {
				tpl = this.getTpl('tplSessionTooltipOver');
			}
		}
		//

		if(!rec) {
			rec = overlayStore.getById(id);
		}
		var type_session_name =  this.type_session_names[rec.get('type_session')];



		Ext.apply(data,rec.data,{
			'type_session_name': type_session_name
		});

		return tpl.apply(data);
	},

	onGridClick:function(e,target){
		var cls = target.getAttribute('class');
		if(cls && cls.indexOf("session") >=0 ){
			this.onSessionClick(e,target);
		}
		
	},

	onSessionClick:function() {
		//console.log('Click on SESSION', arguments);
	},


	setDateRange:function(dateBegin,dateEnd) {
		this.dateBegin = dateBegin;
		//this.setDateRange = dateEnd || dateBegin;
	
		this.updateTmpStore();
		this.updateDataStore();
	},


	updateTmpStore: function(){
		var store = this._tmpStore,
			data =[],
			startDate = this.getStartDate(),// Ext.Date.clearTime(this.dateBegin,true),
			endDate = this.getEndDate();// Ext.Date.clearTime(this.dateEnd,true);

		function Next(){
			startDate.setDate( startDate.getDate() + 1 );
		}

		while(startDate <= endDate) {
			data.push({'date' : Ext.Date.clone(startDate) });
			Next();
		}

		store.loadData(data);

	},

	updateDataStore: function(){
		var store = this.store;
		var filter = [];
		store.clearFilter(true);

		filter.push({
			property: "sess_time_overlaps", 
			value: {
					'time_begin': Ext.Date.format(this.getStartDate(),'Y-m-d H:i:s'),
					'time_end': Ext.Date.format(this.getEndDate(),'Y-m-d H:i:s')
			}
		});

		if(this.cabinetId) filter.push({property: "cabinet_id", value: this.cabinetId });
		if(this.personId) filter.push({property: "person_id", value: this.personId });
		store.filter(filter);
 
	},


	renderTimelineColumn: function (date,cls,rec,rowIdx,columIdx,store,view) {
			var me = this,
				hour = columIdx,
				tbeg,tend,
				minutLength,
				divs=[],
				sessions,
				sesHtml,
				sesionHtmls=[],
				leftOffset,
				sessCls,
				dateMorning,//Утро
				dateEvening//вечер

				;

			sessions = rec.get('sessions');
			cls.tdCls = "time-cel";
			
			dateMorning = Ext.Date.clearTime(date,true);
			dateEvening = Ext.Date.clone(date).setHours(23,59,59,99);

			if(!Ext.isEmpty(sessions) ) {
				sessions.each(function(ses) {
					tbeg = ses.get('time_begin');
					tend = ses.get('time_end');

					if (tbeg.getHours() !== hour)
						return;

					//длина приема в пинутах
					minutLength = (tend - tbeg)* 0.001 / 60;
					leftOffset = tbeg.getMinutes();
					
					sessCls={'class':'', 'style':'' };
					if(ses.phantom) {
						//наложенные данные
						sesHtml = this.renderOverTime(ses,sessCls);	
					}else {
						//оригинал
						sesHtml = this.renderTimelineSession(ses,sessCls);
					}

					// поиск пересечений и рисование красного прямоугольника
					sessions.each(function(overlapSess) {
						if (overlapSess.internalId == ses.internalId)
							return;

						// разница пересечения в минутах
						var diff;

						// ситуация №1 (наложение следующего элемента на конец предыдущего)
						if (overlapSess.get('time_begin') < tbeg && overlapSess.get('time_end') > tbeg) {
							
							diff = (overlapSess.get('time_end') - tbeg) / (1000 * 60);

							sesHtml = '<div class="session-overlap" style="width: '+diff+'px;"></div>' + sesHtml;
							
							var tOver = Ext.Date.format(tbeg, 'H:i') + ' - ' + Ext.Date.format(new Date(overlapSess.get('time_end')-diff+1000), 'H:i');
							ses.set('timeover', tOver);

							return;
						} 

						// для сравнения даты и времени начала
						var s1 = Ext.Date.format(overlapSess.get('time_begin'), 'Y-m-d H:i:s'),
							 s2 = Ext.Date.format(tbeg, 'Y-m-d H:i:s');

						// ситуация №2 (предыдущий элемент полностью внутри)
						if (s1 == s2 && overlapSess.get('time_end') < tend) {
							diff = (overlapSess.get('time_end') - overlapSess.get('time_begin')) / (1000 * 60);

							sesHtml = '<div class="session-overlap" style="width: '+diff+'px;"></div>' + sesHtml;

							var tOver = Ext.Date.format(tbeg, 'H:i') + ' - ' + Ext.Date.format(new Date(overlapSess.get('time_end')-diff+1000), 'H:i');
							ses.set('timeover', tOver);

							return;
						}
						
					}, me);

					sesHtml = '<div data-id="'+ses.get('id')+'" class="session '+sessCls['class']+' " style="width:'+minutLength+'px;left:'+leftOffset+'px;'+sessCls.style+';">'+sesHtml+'</div>';
					sesionHtmls.push(sesHtml);
				}, me);
			}



		/*	if(Ext.isDate(this.overTimeStart) && Ext.isDate(this.overTimeEnd) ){
				if(Ext.Date.between( this.overTimeStart, dateMorning, dateEvening ) ){
					sessCls={'class':'', 'style':'' };
					minutLength = ( this.overTimeStart -  this.overTimeEnd)* 0.001 / 60;
					leftOffset = this.overTimeStart.getMinutes();					
					sesHtml = this.renderOverTime(date,dateMorning, dateEvening, sessCls);
					sesHtml = '<div class="session-over '+sessCls.class+' " style="width:'+minutLength+'px;left:'+leftOffset+'px;'+sessCls.style+';">'+sesHtml+'</div>';

				}

			}*/

			return sesionHtmls.join('');
	},

	//накладываемый прием
	renderOverTime:function(session, cls){
		var editSession = this.session;
		cls['class'] = 'session-current';
		//var innerText = Ext.Date.format(session.get('time_begin'),'H:i')+' - '+Ext.Date.format(session.get('time_end'),'H:i');
		var innerText = '&nbsp;';
		return innerText;
	},

	//рендерим прием
	renderTimelineSession: function(session, cls) {
		var editSession = this.session;
		//var innerText = Ext.Date.format(session.get('time_begin'),'H:i')+' - '+Ext.Date.format(session.get('time_end'),'H:i');
		var innerText = '&nbsp;';
		
		/*if(editSession) {
			if(session.get('id') === editSession.get('id'))	{
				cls.class = 'session-current';
			}
		}*/

		return innerText;
	},

	createDataStore: function() {

		var filter = [];
		var store;

		if(this.departmentId){
			filter.push({
				'property' : 'department_id',
				'value' : this.departmentId
			});
		}

		store = Ext.create('DOCI.data.Store',{
			model: 'MED.EMKU.organize.model.Session',

			storeId: 'organize.employchart',

			extraFilters : filter,
			pageSize: 9999,
			autoLoad: false
		});

		return store;
	},

	/**
	* Временный стор с днями месяца
	*
	* @param {DOCI.data.Store} dataStore стор с основными данными 
	* @return {tmpStore}
	*/
	createTmpStore:function(dataStore){
		var overlayStore, 
			store,
			overlaySessions = this.session || [],
			record,
			genId=0;

		//создаем хранилище с накладываемыми данными
		overlayStore = this.getOverlayStore();

		store = Ext.create('MED.EMKU.organize.component.employchart.EmployStore',{
			sesStore: dataStore,
			overlayStore: overlayStore
			//overlaySessions: this.session
		});

		//console.log('store',store);


	
		
		return store;		
	},

	/**
	 * @param {Number} monthId Номер месяца (0-11)
	 */
	setMonth:function(monthId,year) {
		year = year || this.dateBegin.getFullYear();
		var month = monthId+1;
		var mstr = (month<10)? ('0'+month) : month;
		var d1, d2;	

		this.dateBegin = Ext.Date.parse(year+'-'+ mstr +'-01','Y-m-d');
					
		d1 = this.getStartDate();
		d2 = this.getEndDate();
		//debugger;
		this.setDateRange(d1,d2);
	},



	getRowClass: function(record, rowIndex, rp, ds){
			var date = record.get('date'),
				day = date.getDay();

			if(day === 0 || day === 6) {
				return 'x-session-weekend-row';
			}

			return '';
	},

	renderDateColumn : function(value,cls,record){

		return Ext.Date.format(value,'Y-m-d, D');
	},

	/**
	* Создание стора с накладываемыми данными
	* Используется для удобного доступа к накладываемым панным
	*	
	* @private
	* @return DOCI.data.Store
	*/
	createOverlayStore: function() {
		var store,
			overlaySessions = this.session || [],
			record,
			genId = 0; //счетчик id

		store = Ext.create('DOCI.data.Store',{
			model: 'MED.EMKU.organize.model.Session',
			autoSync: false,
			pageSize: 9999,
			autoLoad: false
		});

		/*Ext.util.Observable.capture(store, function (eventName, args) {	
			console.log(eventName, args);
		});*/

		//Заполняем стор записями
		
		Ext.Array.each(overlaySessions,function(s){
			//генерируем новый id  и присваиваем модели
			s.id = genId++;	
			record = store.model.create(s);
			store.add(record);
		});

		return store;
	},

	/**
	* Стор с накладываемыми данными
	*/
	overlayStore:null,

	/**
	* Возвращает стор с накладываемыми данными
	*/
	getOverlayStore: function() {
		if(!this.overlayStore) {
			this.overlayStore = this.createOverlayStore();
		}
		return this.overlayStore;
	},



	initComponent: function() {

		if(!this.store) {this.store = this.createDataStore();}
		
		var store = this.store;
		var tmpStore = this.createTmpStore(store);
		var me = this;
		this._tmpStore = tmpStore;

		store.on('beforeload',this.dataStoreBeforeLoad,this);
		store.on('load',this.dataStoreAfterLoad,this);

		//активный месяц
		var chMonth= this.dateBegin.getMonth();

		function toggelMonth(button,state){
			if(state) {
				chMonth = button.tag;
				this.setMonth(button.tag);
			}
		}

		this.helpTpl = [
			'<tpl for=".">',
				'<div style="padding:5px;width:300px;">',
					'<h2 aligin="center" >'+this.helpHelp+'</h2>',
					'<hr width="90%" />',
					'<h3>Цветовые обозначения</h3>',
						'<p>&nbsp;&nbsp;&nbsp;<span style="background-color:#6DB3F2;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - '+this.helpTipBlue+'</p>',
						'<p>&nbsp;&nbsp;&nbsp;<span style="background-color:#89F26D;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - '+this.helpTipGreen+'</p>',	
						'<p>&nbsp;&nbsp;&nbsp;<span style="background-color:#f00; opacity: 0.7;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> - '+this.helpTipRed+'</p>',
				'</div>',
			'</tpl>'
		];
		
		var defaultYear = this.dateBegin.getFullYear();

		Ext.apply(this, {
			tbar:[

				{ text: Ext.Date.monthNames[0], pressed: (chMonth===0),  tag:0, toggleGroup:"chartMonth", enableToggle: true,  toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[1], pressed: (chMonth===1),  tag:1, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[2], pressed: (chMonth===2),  tag:2, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[3], pressed: (chMonth===3),  tag:3, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[4], pressed: (chMonth===4),  tag:4, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[5], pressed: (chMonth===5),  tag:5, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[6], pressed: (chMonth===6),  tag:6, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[7], pressed: (chMonth===7),  tag:7, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[8], pressed: (chMonth===8),  tag:8, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[9], pressed: (chMonth===9),  tag:9, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[10], pressed: (chMonth===10),tag:10, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				{ text: Ext.Date.monthNames[11], pressed: (chMonth===11),tag:11, toggleGroup:"chartMonth", enableToggle: true, toggleHandler:toggelMonth ,scope:this },
				'->',
				{ 
					xtype:'numberfield',
					fieldLabel: this.fieldYearText,
					labelWidth: 30, 
					width:100,
					tag:'year',					
					allowDecimals:false,
					minValue:1999,
					maxValue:2999,
					value: defaultYear,
					listeners:{
						change:function(field,value) {
							if(field.isValid()){
								this.setMonth(chMonth,value);	
							}
						},
						scope:this
					}

				},
				Ext.create('DOCI.button.Help', {
					//text: false,
					showType:'hover',
					tipConfig:{
						html: this.helpTpl
					},
					scope: me
				})
			],
			manageOverflow:1,
			layout:'fit',			
			items: {
				border:false,
				xtype:'grid',				
				cls: 'timechart',
				'store': tmpStore,
				columns: [
					{ text: this.colDateText,align:'center', renderer:this.renderDateColumn, scope:this,  dataIndex: 'date', width:120, locked:true, menuDisabled:true, sortable: false},
					{ text: this.colTimeText, 
						menuDisabled:true,
						autoScroll: false,
						sortable: false,
						resizable: false,
						align:'center',	
						
						defaults: { // defaults are applied to items, not the container
							autoScroll: false,
							sortable: false,
							resizable: false,
							width:60,
							menuDisabled:true,
							renderer: this.renderTimelineColumn,
							scope: this,
							dataIndex:'date',
							align:'center'
						},
						columns: [
							{text:'00'},{text:'01'},{text:'02'},{text:'03'},{text:'04'},{text:'05'},{text:'06'},{text:'07'},
							{text:'08'},{text:'09'},{text:'10'},{text:'11'},{text:'12'},{text:'13'},{text:'14'},{text:'15'},
							{text:'16'},{text:'17'},{text:'18'},{text:'19'},{text:'20'},{text:'21'},{text:'22'},{text:'23'}

						]

					},
					
				],



				viewConfig: {					
					getRowClass: this.getRowClass,
					panel:this,

				},

			}

		});

		this.callParent(arguments);
	},

});
