/**
 * @class DOCI.view.Base
 * @extend Ext.panel.Panel
 * @requires DOCI.button.Help
 *
 * Панель-оболочка с тулбаром, в которую встраиваются компоненты линейного списка или дерева (view.Tree, view.Grid)
 * Этот класс является абстрактным.
 *
 * ## Добавление плагинов
 *
 * Если в вашем компоненте необходимо добавить плагин (в виде кнопки в тулбаре) то сделайте следующее:
 * Проноследуйте свой класс от одного из базовых ({@link DOCI.view.Grid} или {@link DOCI.view.Tree} )
 * Например:
 *
 *     Ext.define('MED.EMKU.organize.view.config.Personal', {
 *         extend: 'DOCI.view.Grid',
 *         requires: [
 *             'MED.EMKU.organize.error.OnPerson'
 *         ],
 *         tbarButtons: ['->','myplugin'],
 *         ...
 *         // Ф-ия создания (инициализации) плагина
 *         mypluginButton: function(conf) {
 *             var me = this;
 *
 *             var action = Ext.create('Ext.Action', {
 *                 text: 'Заблокировать таблицу',
 *                 icon: DOCI.icon('lock'),
 *                 handler: me.onMyPLuginButtonClick,
 *                 scope: me
 *             });
 *
 *             // Обязательно нужно добавить кнопку в me._tbarObjects;
 *             me._tbarObjects['plugin_id'] = action;
 *
 *             // и если необходимо в контекстное меню
 *             me._contextMenu.push(action);
 *
 *             // Необходимо вернуть созданную кнопку
 *             return Ext.create('Ext.Button', action);
 *         },
 *
 *         // Обработчик плагина
 *         onMyPLuginButtonClick: function() {
 *             var grid = this.getGrid();
 *             if (grid.isDisabled())
 *                 grid.enable();
 *             else
 *                 grid.disable();
 *         }
 *     });
 *
 * Внимание Ф-ия создания (инициализации) плагина называется mypluginButton не случайно. Название состоит из "Название плагина"+"Button". Если ваш класс не будет иметь ф-ии инициализации плагина или ф-ия не верно названа, то плагин проигнарируется.
 *
 * Некоторые плагины инициализируют события, для того чтобы организовать взаимодействие между компонентами.
 *
 * @author Еременко В.А.
 */
Ext.define('DOCI.view.Base', {
	extend: 'Ext.panel.Panel',
	requires: ['DOCI.button.Help'],
	layout: 'fit',
	defaults: {
		border: false
	},

	/**
	 * Автоматически передавать фокус гриду, при быстрой фильтрации
	 */
	autoSelectOnFilter: true,

	/**
	 * Минимальная ширина панели
	 *
	 * @type {Number}
	 */
	minWidth: 300,

	/**
	 * Текст с помощью
	 * Пример:
	 *
	 *     helpTpl:new Ext.XTemplate(
	 *         '<tpl for=".">',
	 *             '<div style="padding:5px;">',
	 *               '<p><span style="padding-left:10px;background-color:red;" >&nbsp;</span> -  Подсказка</p>',
	 *             '</div>',
	 *         '</tpl>'
	 *     )
	 *
	 * или так
	 *
	 *     helpTpl:'<div style="padding:5px;"><p><span style="padding-left:10px;background-color:red;" >&nbsp;</span> -  OPA</p></div>'
	 *
	 * @type {String|Ext.XTemplate}
	 */
	helpTpl: undefined,

	/**
	 * Набор стандартных кнопок (плагинов) для тулбара панели
	 *
	 * Стандатный набор:
	 *
	 * - `create` - кнопка создания записи;
	 * - `edit` - кнопка редактирования записи;
	 * - `remove` - кнопка удаления записи;
	 * - `reload` - кнопка обновления древовидного списка (только для view.Tree)
	 * - `search` - поле быстрого поиска;
	 * - `filter` - кнопка вызова диалога фильтрации;
	 * - `removeFilter` - кнопка удаления фильтра;
	 * - `help` - отображения помощи;
	 * - значения `-`, `->`, ` ` - используются как разделители кнопок.
	 *
	 * Каждая кнопка должна иметь ф-ию создания имеющую название <имя_кнопки><Button>
	 * Например для создания кнопок:
	 *
	 * - `search` используется {@link DOCI.view.Base#searchButton}
	 * - `help` используется {@link DOCI.view.Base#helpButton}
	 * - `create` используется {@link DOCI.view.Base#createButton}
	 *	*
	 * @type {String[]}
	 */
	tbarButtons: ['create','edit','remove','->','search','filter','removeFilter','help'],

	/**
	 * Имя поля передоваемое в фильтр, если будет использоваться плагин 'search'
	 *
	 * @type {String}
	 */
	searchField:'name',

	/**
	 * Текст отображаемый в пустом поле плагина 'search'
	 *
	 * @type {String}
	 */
	searchText:null,

	/**
	 * Заранее предустановленный фильтр
	 * Пример:
	 *
	 *     filter: {
	*         'fam':'Ivanov'
	*         'age':'15'
	*     }
	 *
	 * @type {Object}
	 */
	filter: null,

	/**
	 * Не изменяемые значения фильтра.
	 * Даже нажав на кнопку отчистить фильтр. Эти значения не отчистятся
	 * Конечно лучше задать extraFilter для Store , однако возможны разные ситуации
	 *
	 * @type {Object}
	 */
	extraFilter: null,

	/**
	 * Форма фильтрации.
	 * Форма будет помещена в диалог и показана при нажатии на кнопку 'filter' см. {@link #tbarButtons}
	 *
	 * @type {Object|DOCI.form.Form}
	 */
	filterForm: null,

	/**
	 * Настройки передаваемые диалогу фильтрации при его создании
	 * @type {Object}
	 */
	defaultFilterDlg: null,

	/**
	 * Созданный и проинициализированный диалог фильтрации
	 *
	 * @private
	 * @type {DOCI.window.FormDialog}
	 */
	_filterDlg: null,

	/**
	 * Экземпляр контекстного меню
	 *
	 * @private
	 * @type {Ext.menu.Menu}
	 */
	_contextMenu: null,

	/**
	 * Объект с компонентами панели инструментов (для поиска и манипуляции)
	 *
	 * @private
	 * @type {Object}
	 */
	_tbarObjects: null,

	/**
	 * @event beforefilterchange
	 * Вызывается перед применением фильтра
	 * @param {DOCI.view.Grid} grid Грид
	 * @param {Object} filter Новый фильтр
	 * @param {Object} oldFilter Старый фильтр
	 */

	/**
	 * @event filterchange
	 * Вызывается при изменении фильтра
	 * @param {DOCI.view.Grid} grid Грид
	 * @param {Object} filter Новый фильтр
	 * @param {Object} oldFilter Старый фильтр
	 */

	/**
	 * @event beforeselect
	 * Вызывается перед выделениием записи
	 * @param {Ext.selection.Model} rowModel Модель выделения
	 * @param {Ext.data.Model} record Запись
	 * @param {Number} index номер записи
	 */

	/**
	 * @event select
	 * Вызывается при выделении записи
	 */

	/**
	 * @event deselect
	 * Вызывается при снятии выделения с записи
	 */

	/**
	 * @event createbuttonclick
	 * Вызывается при нажатии на кнопку 'create'
	 */

	/**
	 * @event editbuttonclick
	 * Вызывается при нажатии на кнопку ' edit'
	 */

	/**
	 * @event removebuttonclick
	 * Вызывается при нажатии на кнопку 'remove'
	 */

	/**
	 * @event searchbuttonclick
	 * Вызывается при нажатии на кнопку 'search'
	 */

	/**
	 * @event filterbuttonclick
	 * Вызывается при нажатии на кнопку 'filter'
	 */

	/**
	 * @event removefilterbuttonclick
	 * Вызывается при нажатии на кнопку 'removefilter'
	 */

	///
	txtFltr: 'Фильтр ',
	txtFind: 'Поиск ',
	txtClear: 'Очистить ',
	txtCreate: 'Создать ',
	txtDel: 'Удалить ',
	txtChange: 'Изменить ',
	txtFastFind: 'Расширенный поиск ',
	txtClearFltr: 'Очистить фильтр ',
	txtNextPage: 'След.стр. ',
	txtPrevPage: 'Пред.стр. ',
	txtRefresh: 'Обновить ',
	///


	initComponent: function() {
		var me = this;
		me.hKey = DOCI.HotKeys;
		me.bTip = me.hKey.treeGridKeys;

		Ext.define('Ext.PagingToolbarTip', {
			override: 'Ext.PagingToolbar',
			prevText: me.txtPrevPage+me.hKey.render(me.bTip.goPgUp),
			nextText: me.txtNextPage+me.hKey.render(me.bTip.goPgDn),
			refreshText: me.txtRefresh+me.hKey.render(me.bTip.refreshTab),
		});

		// создаем диалог с формой фильтрации
		for (var i in me.tbarButtons) {
			if (me.tbarButtons[i] == 'filter' && !Ext.isEmpty(me.filterForm)) {
				me.createFilterDlg();
				break;
			}
		}

		me.addEvents(['beforefilterchange', 'filterchange', 'select', 'deselect',
			'createbuttonclick', 'editbuttonclick', 'removebuttonclick', 'searchbuttonclick',
			'filterbuttonclick', 'removefilterbuttonclick', 'beforeselect']);

		Ext.applyIf(me,{
			// инициализируем тулбар
			tbar: me.createToolbar()
		});

		me.callParent(arguments);

		me.store.on('remove', me.afterRemoveItem, me);
		me.store.on('add', me.afterAddItem, me);
		me.store.on('insert', me.afterAddItem, me);
		me.store.on('beforeload', me.beforeLoadData, me);
	},

	/**
	 * Создает объект диалога фильтрации и помещает его в переменную {@link #_filterDlg}.
	 * Для переопределения  настроек диалога, задайте их в объекте конфигурации диалога {@link #defaultFilterDlg}
	 *
	 * @private
	 */
	createFilterDlg: function() {
		var me = this, fconfig = this.defaultFilterDlg || {};

		Ext.applyIf(fconfig,{
			resizable: false,
			title: me.txtFltr,
			okBtnText: me.txtFind,
			iconCls: 'doci-icon-funnel-16',
			width: 450,
			items: [
				me.filterForm
			],
			fBarButtons: [
				{
					text: me.txtClear,
					key: me.hKey.formKeys.clearFormFields,
					tooltip: me.hKey.render(me.hKey.formKeys.clearFormFields),
					handler: function() { me.filterForm.getForm().reset(); this.focus(false, 100);}
				}, '->', 'ok', 'cancel'
			],
			listeners: {
				ok: function(dlg, form, values) {
					me.setFilter(values);
					me._filterDlg.hide();
				},
				cancel: function(dlg) {
					me._filterDlg.hide();
					me.focus();
					me.setFocus();
				},
				show: function(dlg) {
					var filter = me.getFilter(),
						form = dlg.down('form').getForm(),
						searchCmp = me.buttonCmpGet('search');

					// #806
					if (!Ext.isEmpty(searchCmp) && !Ext.isEmpty(me.searchField) && !Ext.isEmpty(searchCmp.getValue()))
						filter[me.searchField] = searchCmp.getValue();

					form.reset();
					form.setValues(filter);
				}
			}

		});

		me._filterDlg = Ext.create('DOCI.window.FormDialog', fconfig);

		var dlgForm = me._filterDlg.items.getAt(0);
		if (dlgForm.isBaseForm) {
			dlgForm.getForm().trackResetOnLoad = false;
			dlgForm.on('enterkey', function(form, values) {
				me.setFilter(values);
				me._filterDlg.hide();
			});
			dlgForm.on('esckey', function() { me._filterDlg.hide(); });
		}
	},

	/**
	 * Возвращает объект диалога фильтрации
	 *
	 * @return {DOCI.window.FormDialog}
	 */
	getFilterDlg: function() {
		return this._filterDlg;
	},

	/**
	 * Создание панели инструментов с кнопками и контекстным меню
	 *
	 * @private
	 * @return {Ext.toolbar.Toolbar}
	 */
	createToolbar: function() {
		var me = this,
			items = [];

		me._contextMenu = [];
		me._tbarObjects = {};
		var i = 0;

		Ext.each(me.tbarButtons, function(v) {
			// если строка - то стандартный элемент
			var tag = v;

			if (typeof v == 'string') {
				if (['-', '->', ' ', '|'].indexOf(v) >= 0) {
					items.push(v);
					i += 1;
					return;
				}
				var f = me[v + 'Button'];
				if (Ext.isFunction(f)) {
					v = f.call(me);
					items.push(v);
				}
				// иначе, если объект и есть xtype - то добавляем "как есть"
			} else if (typeof v == 'object' && v.xtype !== undefined) {
				items.push(v);
			}

			if (items[i])
                items[i].tag = tag;

			i += 1;
		}, me);

		var tBar = null;
		if (items.length > 0) {
			tBar = Ext.create('Ext.toolbar.Toolbar', {
				'items': items,
				'id': me.getId()+'-tbar'
			});
			me._contextMenu = me.createContextMenu();
		}
		return tBar;
	},

	/**
	 * Возвращает объект панели инструментов по названию плагина
	 * Не рекомендуется использовать.
	 *
	 * @private
	 * @deprecated
	 *
	 * @return {Ext.Component}
	 */
	getTbarItemId: function(id_suffix) {
		id_suffix = id_suffix || Ext.id();
		return this.getId() + '-' + id_suffix;
	},

	/**
	 * Создает контекстное меню
	 *
	 * @private
	 * @return {Ext.menu.Menu}
	 */
	createContextMenu: function() {
		var me = this;
		if (me._contextMenu.length > 0) {
			return Ext.create('Ext.menu.Menu', {
				items: me._contextMenu
			});
		} else {
			return null;
		}
	},

	/**
	 * получаем объект контекстного меню
	 *
	 * @return {Ext.menu.Menu}
	 */
	getContextMenu: function() {
		return this._contextMenu;
	},

	/**
	 * поиск кнопки по id-суфиксу
	 *
	 * @return {Ext.Component}
	 */
	buttonCmpGet: function(id_suffix){
		return this._tbarObjects[id_suffix];
	},

	/**
	 * Сравнение двух фильтров на наличие разных параметров (на идентичность)
	 *
	 * @param {Object} o1 первый объект (фильтр)
	 * @param {Object} o2 первый объект (фильтр)
	 *
	 * @return {Boolean} Возвращает `true`, если объекты идентичны
	 */
	compareFilter: function (o1, o2){
		o1 = o1 || {};
		o2 = o2 || {};

		for (var p in o1) {
			if (o1[p] !== o2[p]) {
				return false;
			}
		}

		for (var p in o2) {
			if (o1[p] !== o2[p]) {
				return false;
			}
		}

		return true;
	},

	/**
	 * Отчистка фильтра без перезагрузки данных
	 */
	clearFilter: function() {
		this.filter = {};
	},

	/**
	 * Функция для предобработки фильтра - переопределить в своем классе, если необходимо
	 *
	 * @param {Object} filter Фильтр
	 * @return {Object} Обработанный фильтр
	 */
	preProcessFilter: function(filter) {
		return filter;
	},

	/**
	 * Установка списка фильтров (`autoSelect = false` для отмены автоматического выделения записи)
	 * Фильтр примется к стору и после загрузки данных фокус уйдет на первую запись
	 * Фильтр применяется только в случае если текущий фильтр отличается от нового
	 * Перед применением будет вызван метод {@link #preProcessFilter } и событие beforefilterchange
	 *
	 * @param {Object} filter применяемый фильтр
	 * @param {Boolean} autoSelect Флаг автоматической установки фокуса на первую запись
	 */
	setFilter: function(filter, autoSelect) {
		var oldFilter = this.filter;
		filter = (filter === null) ? {} : filter;

		if (DOCI.isEmptyObject(filter) && DOCI.isEmptyObject(oldFilter)) return;

		// удаление пустых полей фильтра
		for (var key in filter) {
			if (Ext.isEmpty(filter[key])) delete filter[key];
		}

		// вызов предобработчика
		filter = this.preProcessFilter(filter);

		// проверка, изменился ли фильтр
		if (!this.compareFilter(filter, oldFilter)) {

			if (this.fireEvent('beforefilterchange', this, filter, oldFilter) === false)
				return;

			this.fireEvent('filterchange', this, filter, oldFilter);
			this.filter = filter;
		}
		// принудительно применяем фильтр, даже если он не изменился (#705, вопрос №2)
		this.applyFilter(filter, autoSelect);
	},

	/**
	 * Применение фильтра к стору (переписать в соответствии с требованиями)
	 *
	 * @param {Object} filter применяемый фильтр
	 * @param {Boolean} autoSelect Флаг автоматической установки фокуса на первую запись
	 */
	applyFilter: function(filter, autoSelect) {
		var me = this,
			removeFilter_button = this.buttonCmpGet('removeFilter'),
			store = this.store,
			store_filters = [];

		if (removeFilter_button) {
			if (this.compareFilter(filter, {})) {
				removeFilter_button.setDisabled(true);
			} else {
				removeFilter_button.setDisabled(false);
			}
		}

		// добавляем экстра-фильтр
		if (this.extraFilter) {
			var merge = Ext.clone(this.extraFilter);
			Ext.applyIf(merge, filter);
			filter = merge;
		}

		// создаем объекты фильтра и применяем к стору
		Ext.Object.each(filter, function(k, v) {
			store_filters.push(
				Ext.create('Ext.util.Filter', {property: k, value: v})
			);
		});
		store.clearFilter(true);
		store.filter(store_filters);

		// подвешиваем событие на загрузку стора для выделения записи, если разрешено
		if (autoSelect === undefined || autoSelect === true) {
			store.on('load', function(store, records, successful) {
				// ставим фокус в строку быстрого поиска
				me.setSearchFocus();
			}, this, {single: true, delay: 100});
		}
	},

	/**
	 * Получить список фильтров
	 * @return {Object}
	 */
	getFilter: function() {
		return Ext.clone(this.filter) || {};
	},

	/**
	 * Функция возврата фокуса в панель обязательно переопределить иначе не работает
	 * Пример на основе дерева:
	 *
	 *     var view = tree.getTree().getView();
	 *     view.focus(false, true);
	 *
	 */
	setFocus: Ext.emptyFn,

	/**
	 * Выбрать элемент и передать фокус
	 *
	 * @param {Ext.data.Model} record запись
	 * @param {Boolean} isfocused флаг установки фокуса
	 */
	select: function(record, isfocused) {
		// процесс выделения элемента описан в потомках
		if (isfocused) {
			this.setFocus();
		}
	},

	/**
	 * Ф-ия отработает после удаления записи из стора
	 * @return {void}
	 */
	afterRemoveItem: function() {
		return;
	},

	/**
	 * Ф-ия отработает после добавления записи в стор
	 *
	 */
	afterAddItem: function() {
		return;
	},

	/**
	 * Функция выполняется перед загрузкой данных в хранилище (сбрасываем состояние кнопок)
	 * @return {[type]} [description]
	 */
	beforeLoadData: function() {
		this.onRecordDeselect();
	},

	/**
	 * функция срабатывает перед выделением записи
	 * @param {uniknow} rowModel модель строчки
	 * @param {Ext.data.Model} record запись
	 * @param {Number} index индекс в сторе
	 */
	beforeSelect: function(rowModel, record, index) {
		return this.fireEvent('beforeselect', rowModel, record, index);
	},

	/**
	 * кнопка создания записи
	 *
	 * @return {Ext.Button}
	 */
	createButton: function() {
		var me = this;

		var action = Ext.create('Ext.Action', {
			text: me.txtCreate,
			iconCls: 'doci-icon-add-16',
			handler: me.onCreateButton,
			tooltip: me.txtCreate + me.hKey.render(me.bTip.addNewRecord),
			scope: me
		});
		me._tbarObjects['create'] = action;
		me._contextMenu.push(action);

		return Ext.create('Ext.Button', action).setText('');
	},

	/**
	 * кнопка удаления записи
	 *
	 * @return {Ext.Button}
	 */
	removeButton: function() {
		var me = this;

		var action = Ext.create('Ext.Action', {
			text: me.txtDel,
			iconCls: 'doci-icon-delete-16',
			handler: me.onRemoveButton,
			tooltip: me.txtDel + me.hKey.render(me.bTip.delRecord),
			scope: me
		});
		me._tbarObjects['remove'] = action;
		me._contextMenu.push(action);

		return Ext.create('Ext.Button', action).setText('');
	},

	/**
	 * кнопка редактирования записи
	 * @return {Ext.Button}
	 */
	editButton: function() {
		var me = this;

		var action = Ext.create('Ext.Action', {
			text: me.txtChange,
			iconCls: 'doci-icon-pencil-16',
			handler: me.onEditButton,
			tooltip: me.txtChange + me.hKey.render(me.bTip.editRecord),
			scope: me
		});
		me._tbarObjects['edit'] = action;
		me._contextMenu.push(action);

		return Ext.create('Ext.Button', action).setText('');
	},

	/**
	 * триггерное поле быстрого поиска
	 * @return {Ext.form.field.Trigger}
	 */
	searchButton: function() {
		var me = this;
		var button = Ext.create('Ext.form.field.Trigger', {
			triggerCls : Ext.baseCSSPrefix + 'form-search-trigger',
			style: 'margin-left: 5px',
			allowBlank: true,
			tooltip: me.txtFastFind + me.hKey.render(me.bTip.focusSearchField),
			emptyText: me.searchText || me.txtFind,
			onTriggerClick: function() {
				me.onSearchButton(this, this.getValue());
			},
			listeners:{
				specialkey:function(f, e){
					var me = this;
					if (e.getKey() == e.ENTER) me.onTriggerClick();
					if (e.getKey() == e.ESC) me.setValue(null);
				}
				// закоментировал, т.к. в случае обработки ошибки бесконечно происходит переход от закрытого диалога к полю (из-за активации последнего активного компонента)
                /*blur:function(){
                this.onTriggerClick();
                }*/
			}
		});
		me._tbarObjects['search'] = button;
		me.on('filterchange', function(grid, filter) {
			if (me.searchField) {
				var v = filter[me.searchField];
				me.buttonCmpGet('search').setValue(v);
			}
		});

		return button;
	},

	/**
	 * кнопка создания фильтра
	 *
	 * @return {Ext.Button}
	 */
	filterButton: function() {
		var me = this;

		var action = Ext.create('Ext.Action', {
			text: me.txtFltr,
			iconCls: 'doci-icon-funnel-16',
			handler: me.onFilterButton,
			tooltip: me.txtFastFind + me.hKey.render(me.bTip.focusSearchBut),
			scope: me
		});
		me._tbarObjects['filter'] = action;
		me._contextMenu.push(action);

		return Ext.create('Ext.Button', action).setText('');
	},

	/**
	 * кнопка удаления (очистки) фильтра
	 * @return {Ext.Button}
	 */
	removeFilterButton: function() {
		var me = this;

		var action = Ext.create('Ext.Action', {
			text: me.txtClearFltr,
			iconCls: 'doci-icon-funnel-minus-16',
			hidden: false,
			disabled: true,
			handler: me.onRemoveFilterButton,
			tooltip: me.txtClearFltr + me.hKey.render(me.bTip.clearTabFilter),
			scope: me
		});

		me._tbarObjects['removeFilter'] = action;
		me._contextMenu.push(action);

		return Ext.create('Ext.Button', action).setText('');
	},

	/**
	 * кнопка помощи
	 * @return {DOCI.button.Help}
	 */
	helpButton: function() {
		var me = this,tpl=this.helpTpl;

		if (Ext.isEmpty(tpl)) return false;

		var button = Ext.create('DOCI.button.Help', {
			showType: 'hover',
			tipConfig: {
				html: this.helpTpl
			},
			scope: me
		});

		return button;
	},


	/* Обработчики действий */


	/**
	 * Ф-ия вызывается при каждом выборе записи и активирует кнопки редактирования и удаления
	 * @param {Ext.data.Model} rec
	 */
	onRecordSelect: function(rec) {
		var edit_button = this.buttonCmpGet('edit'),
			remove_button = this.buttonCmpGet('remove');

		if (edit_button) edit_button.setDisabled(false);
		if (remove_button) remove_button.setDisabled(false);

		this.fireEvent('select', this, rec);
	},

	/**
	 * Ф-ия вызывается при каждом выборе записи и деактивирует кнопки редактирования и удаления
	 * (сброс состояний кнопок)
	 */
	onRecordDeselect: function() {
		var edit_button = this.buttonCmpGet('edit'),
			remove_button = this.buttonCmpGet('remove');

		if (edit_button) edit_button.setDisabled(true);
		if (remove_button) remove_button.setDisabled(true);

		this.fireEvent('deselect', this);
	},

	/**
	 * Нажата кнопка создать ('create')
	 */
	onCreateButton: function() {
		this.fireEvent('createbuttonclick', this);
	},

	/**
	 * Нажата кнопка 'remove'
	 */
	onRemoveButton: function() {
		var me = this;
		me.fireEvent('removebuttonclick', this);
	},

	/**
	 * Нажата кнопка 'edit'
	 */
	onEditButton: function() {
		this.fireEvent('editbuttonclick', this);
	},

	/**
	 * Нажата кнопка 'search'
	 *
	 * @param {Ext.form.field.Trigger} field Поле ввода строки поиска см. {@link #searchButton}
	 */
	onSearchButton: function(field, val) {
		var me = this;
		if (me.searchField) {
			var f = me.getFilter();
			f[me.searchField] = val;

			// Сброс ID фильтра в null для #1032 http://redmine.doci.in.ua/issues/1032
			if (!Ext.isEmpty(f.id))
				delete f.id;

			me.setFilter(f, me.autoSelectOnFilter);
		}
		me.fireEvent('searchbuttonclick', me, val, field);
	},

	/**
	 * Нажата кнопка 'filter'
	 */
	onFilterButton: function() {
		var me = this;
		this.fireEvent('filterbuttonclick', this);
		if (!this.filterForm) return;
		me._filterDlg.show(this);
	},

	/**
	 * Нажата кнопка 'removefilter'
	 */
	onRemoveFilterButton: function() {
		var me = this;
		me.setFilter({});
		var rFB = me.buttonCmpGet('removeFilter');
		rFB.setDisabled(true);

		me.fireEvent('removefilterbuttonclick', me);
	},

	/**
	 * Фокус на первую строку активной таблицы
	 * @type {Function}
	 */
	selectFirstRow: function(){
		var me = this;

		if (me.store) {
			me.store.on('load', function() {
				me.setFocus();
			});
		}
	},

	//private
	afterRender:function(){
		var me = this;
		
		me.callParent(arguments);
		
		// Hotkeys
		me.initHotKeys();
	},

	/**
	 * Инициализация глобальных клавиш для
	 * @type {Function}
	 */
	initHotKeys:function(){
		var me = this;
		//Hotkeys

		var keyMap = me.getKeyMap();
		var HK = DOCI.HotKeys.treeGridKeys;
		/* Устанавливает фокус на первую кнопку верхнего тулбара */
		keyMap.on(HK.focusFirst, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var fBut = me.down('toolbar[dock="top"] button[disabled="false"]');
			if (fBut) fBut.focus('', 10);
		}), me);

		/* Устанавливает фокус в поле поиска(фильтра) верхнего тулбара */
		keyMap.on(HK.focusSearchField, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var sf = me.down('toolbar[dock="top"] triggerfield');
			if (sf) sf.focus('', 10);
		}), me);

		/* Нажимает на кнопку поиска(фильтра) верхнего тулбара */
		keyMap.on(HK.focusSearchBut, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var sf = me.down('toolbar[dock="top"] button[tag="filter"]');
			if (sf) me.onFilterButton();
		}), me);

		/* Вызывает событие при нажатии на кнопку Очистить фильтр (верхний тулбар) */
		keyMap.on(HK.clearTabFilter, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var ctf = me.down('toolbar[dock="top"] button[tag="removeFilter"]');
			if (ctf && !ctf.hidden && !ctf.disabled) {
				me.onRemoveFilterButton();
				me.selectFirstRow();
			}
		}), me);

		/* Вызывает событие при нажатии на кнопку Новая запись (верхний тулбар) */
		keyMap.on(HK.addNewRecord, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var anr = me.down('toolbar[dock="top"] button[tag="create"]');
			if (anr && !anr.disabled) me.onCreateButton();
		}), me);

		/* Вызывает событие при нажатии на кнопку "Обновить" на нижнем или верхнем тулбаре */
		keyMap.on(HK.refreshTab, (function(key, evt) {
			evt.stopEvent();
			var me = this;
			//Для дерева
			var rb = me.down('toolbar[dock="top"] button[tag="reload"]');
			if (rb && !rb.disabled) {
				me.onReloadButton();
			}
			//Для таблицы
			if (!rb) rb = me.down('toolbar[dock="bottom"] button[itemId="refresh"]');
			if (rb && !rb.disabled && !me.tree) {
				me.getGrid().store.reload();
				me.selectFirstRow();
			}
		}), me);

		/* Вызывает событие при нажатии на кнопку На страницу Вперед (Paging Bar) */
		keyMap.on(HK.goPgDn, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var rb = me.down('toolbar[dock="bottom"] button[itemId="next"]');
			if (rb && !rb.disabled) {
				var bBar = rb.up();
				bBar.moveNext();
				me.selectFirstRow();
			}
		}), me);

		/* Вызывает событие при нажатии на кнопку На страницу Назад (Paging Bar) */
		keyMap.on(HK.goPgUp, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var rb = me.down('toolbar[dock="bottom"] button[itemId="prev"]');
			if (rb && !rb.disabled) {
				var bBar = rb.up();
				bBar.movePrevious();
				me.selectFirstRow();
			}
		}), me);

		/* Устанавливает фокус на поле ввода страницы (Paging Bar) */
		keyMap.on(HK.focusPageField, (function(key, evt) {
			var me = this;
			evt.stopEvent();
			var pf = me.down('toolbar[dock="bottom"] field[itemId="inputItem"]');
			if (pf) pf.focus('', 10);
		}), me);

		//Сохранение
		// keyMap.on(me.hKey.adminKeys.ok, (function(key, evt) {
		// 	evt.stopEvent();
		// 	var buts = Ext.query('button[data-qtip="'+me.hKey.render(me.hKey.adminKeys.ok)+'"]');
		// 	for (var i in buts) {
		// 		var id = buts[i].id;
		// 		var sid = id.substring(0, id.length-6);
		// 		var cB=Ext.getCmp(sid);
		// 		if (cB.disabled==false) {
		// 			cB.handler.call(cB.scope || cB, cB);
		// 		}
		// 	}
		// }), me);

		var elm = (me.tree?me.getTree().view.getEl():me.getGrid().view.getEl());
		elm.addKeyMap({
			eventName: 'keydown',
			binding: [
				/*Редактирование*/
				{
					ctrl: HK.editRecord.ctrl,
					alt: HK.editRecord.alt,
					shift: HK.editRecord.shift,
					key: HK.editRecord.key,
					fn: function(num, evt){
						//console.log(evt);
						//var me = this;
						evt.stopEvent();
						me.onEditButton();
					}
				},
				/*Сохранение*/
				{
					ctrl: me.hKey.adminKeys.ok.ctrl,
					alt: me.hKey.adminKeys.ok.alt,
					shift: me.hKey.adminKeys.ok.shift,
					key: me.hKey.adminKeys.ok.key,
					fn: function(num, evt){
						evt.stopEvent();
						if (me.up() && me.up()._okBtnId) {
							var oB=Ext.getCmp(me.up()._okBtnId);
							if (oB.disabled===false) {
								oB.handler.call(oB.scope || oB, oB);
							}
						}
					}
				},
				/*Удаление*/
				{
					ctrl: HK.delRecord.ctrl,
					alt: HK.delRecord.alt,
					shift: HK.delRecord.shift,
					key: HK.delRecord.key,
					fn: function(num, evt){
						evt.stopEvent();
						var db = me.down('toolbar[dock="top"] button[tag="remove"]');
						if (db && !db.disabled) {
							me.onRemoveButton();
						}
					}
				},
				/*Вперед на 1 страницу PgDn*/
				{
					ctrl: HK.goPgDn.ctrl,
					alt: HK.goPgDn.alt,
					shift: HK.goPgDn.shift,
					key: HK.goPgDn.key,
					fn: function(num, evt){
						evt.stopEvent();
						var rb = me.down('toolbar[dock="bottom"] button[itemId="next"]');
						if (rb && !rb.disabled) {
							var bBar = rb.up();
							bBar.moveNext();
							me.selectFirstRow();
						}
					}
				},
				/*Назад на 1 страницу PgDn*/
				{
					ctrl: HK.goPgUp.ctrl,
					alt: HK.goPgUp.alt,
					shift: HK.goPgUp.shift,
					key: HK.goPgUp.key,
					fn: function(num, evt){
						evt.stopEvent();
						var rb = me.down('toolbar[dock="bottom"] button[itemId="prev"]');
						if (rb && !rb.disabled) {
							var bBar = rb.up();
							bBar.movePrevious();
							me.selectFirstRow();
						}
					}
				}
			]
		});

	},

	// private
	getKeyMap: function() {
		var me = this;
		var el = me.getEl();

		return me.keyMap || (me.keyMap = new Ext.util.KeyMap({
			target: el
		}));
	},

	selRow: true,

	//#1032
	setSearchFocus: function() {
		// реализация в Grid и в Tree соответственно
	}
});
