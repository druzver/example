/**
 * @class DOCI.view.Tree
 * @extend DOCI.view.Base
 *
 * Компонент позволяющий организовать работу с линейными списками
 *
 * Пример:
 *
 *     var store = Ext.create('DOCI.data.Store',{
 *         ...
 *     });
 *
 *     Ext.create('DOCI.view.Tree',{
 *         tbarButtons : ['->','search'],
 *         searchField : 'fam_in',
 *         searchText : 'Фамилия',
 *         defaultGrid : {
 *             pager : true,
 *             columns : [
 *                 {
 *                     xtype: 'treecolumn',
 *                     text : 'Фамилия',
 *                     dataIndex : 'fam'
 *                 },{
 *                     text : 'Имя',
 *                     dataIndex : 'im'
 *                 },{
 *                     text : 'Отчество',
 *                     dataIndex: 'ot'
 *                 },
 *             ]
 *         }
 *     });
 *
 * @author Еременко В.А.
 */
Ext.define('DOCI.view.Tree', {
    extend: 'DOCI.view.Base',
    layout: 'card',

    selRow: false,

    /**
     * Выделенная запись в списке.
     * Применяется для установки автоматического фокуса на указанную запись
     *
     * @type {Ext.data.Model}
     */
    record: null,

    /**
     * Хранилище записей
     *
     * @type {DOCI.data.Store}
     */
    store: null,

    /**
     * Флаг, указывающий на то куда загружать результаты применения фильтра. Если равен `true`, то при фильтрации компонент древовидного списка подменяется на компонент линейного списка
     * @type {Boolean}
     */
    filterToGrid: true,

    /**
     * Флаг, указывающий на то будет ли в отфильтрованном гриде показаны только листья дерева
     *
     * true  - только листья дерева
     * false - и листья и папки
     *
     * @type {Boolean}
     */
    onlyLeaf: true,

    /**
     * Имя поля фильтрации отправляемое на сервер при оnlyLeaf = true
     *
     * @type {String}
     */
    onlyLeafFilterName: 'leaf',

    /**
     * Модель данных для хранилища грида (если используется фильтрация в грид)
     * @type {String}
     */
    gridStoreModel: undefined,

    // содержимое панели инструментов (переопределение дополнительной кнопкой reload)
    tbarButtons: ['create', 'edit', 'remove', 'reload', '->', 'search', 'filter', 'removeFilter', 'help'],

    /**
     * Объект настроек грида (колонки,пейджер и.т.д) см. {@link Ext.grid.Panel}
     * Пример:
     *
     *     defaultGrid: {
	 *         pager: true,
	 *         columns: [
	 *             {
	 *                 xtype: 'checkcolumn',
	 *                 header: ' ',
	 *                 dataIndex: 'on_schedule',
	 *                 width: 30,
	 *                 resizable: false,
	 *                 menuDisabled: true,
	 *                 hideable: false,
	 *                 editor: {
	 *                     xtype: 'checkbox',
	 *                     cls: 'x-grid-checkheader-editor'
	 *                 },
	 *                 listeners: {
	 *                     'checkchange': me.onChangeRecord,
	 *                     scope: me
	 *                 }
	 *             },{
	 *                 text: 'Фамилия',
	 *                 flex: 1,
	 *                 dataIndex: 'fam'
	 *             },{
	 *                 text: 'Имя',
	 *                 flex: 1,
	 *                 dataIndex: 'im'
	 *             },{
	 *                 text: 'Отчество',
	 *                 flex: 1,
	 *                 dataIndex: 'ot'
	 *             }
	 *         ]
	 *     }
     *
     * @type {Object}
     */
    defaultGrid: undefined, // конфигурация таблицы

    /**
     * Объект настроек древовидного списка (колонки и др.) см. {@link Ext.tree.Panel}
     * Пример:
     *
     *     defaultTree: {
	 *         pager: true,
	 *         columns: [
	 *             {
	 *                 xtype: 'treecolumn',
	 *                 dataIndex: 'on_schedule',
	 *                 width: 30,
	 *                 resizable: false,
	 *                  menuDisabled: true
 	 *             },{
	 *                 text: 'Фамилия',
	 *                 flex: 1,
	 *                 dataIndex: 'fam'
	 *             },{
	 *                 text: 'Имя',
	 *                 flex: 1,
	 *                 dataIndex: 'im'
	 *             },{
	 *                 text: 'Отчество',
	 *                 flex: 1,
	 *                 dataIndex: 'ot'
	 *             }
	 *         ]
	 *     }
     *
     * @type {Object}
     */
    defaultTree: undefined, // конфигурация таблицы

    // внутреннее хранилище для хранения результатов фильтрации,
    // если они отображаются в линейный список
    _intStore: null,

    ///
    txtRefresh: 'Обновить',
    txtRefreshTree: 'Перезагрузить дерево',
    ///


    initComponent: function(conf) {
        var me = this;
        me.hKey = DOCI.HotKeys;
        me.bTip = DOCI.HotKeys.treeGridKeys;

        me.tag = 'TreeView';
        // пустой объект конфигурации
        Ext.applyIf(me, {
            defaultTree: {},
            filterToGrid: true,
            gridFocused: false,
            treeFocused: false,
            // отложенное задание сброса флага фокуса
            unfocusGridTask: new Ext.util.DelayedTask(function() {
                me.gridFocused = false;
            }),
            unfocusTreeTask: new Ext.util.DelayedTask(function() {
                me.treeFocused = false;
            })
        });

        // задание начальных значений дерева
        Ext.applyIf(me.defaultTree, {
            allowDirSelect: false,	// разрешить выбор папок
            collapsible: false,
            multiSelect: false,
            rootVisible: true,
            useArrows: false,
            singleExpand: false,
            border: false,
            columns: []
        });

        me.tree = me.createTree();

        // если используется grid для результата фильтрации
        if (me.filterToGrid) {
            // create internal store
            me._intStore = Ext.create('DOCI.data.Store', {
                model: (me.gridStoreModel === undefined) ? me.store.model.modelName : me.gridStoreModel
            });
            // пустой объект конфигурации дерева
            Ext.applyIf(me, {
                defaultGrid: {}
            });
            // задание начальных значений таблицы
            Ext.applyIf(me.defaultGrid, {
                multiSelect: false,
                border: false,
                pager: false,
                columns: []
            });
            me.grid = me.createGrid();
            me.items = [ me.tree, me.grid ];
        } else {
            me.items = [ me.tree ];
        }

        me.callParent([conf]);

        me.tree.store.on('beforeload', me.onTreeBeforeLoad, me);
        me.tree.store.on('load', me.onTreeAfterLoad, me);
        me.tree.on('select', me.onRecordSelect, me);
        me.tree.on('deselect', me.onRecordDeselect, me);
        me.tree.on('beforeselect', me.beforeSelect, me);
        me.tree.on('itemcollapse', me.onItemCollapse, me);
        me.tree.on('itemdblclick', me.onItemDblClick, me);
        me.tree.on('afterrender', function(tree) {
            var el = tree.getView().el;
            el.on('focus', this.onTreeFocus, this);
            el.on('blur', this.onTreeBlur, this);

            var topLink = Ext.DomHelper.insertFirst(tree.el, '<a id="' + me.tree.id + '-top-link" class="doci-hidden-focus-link" href="#top">top</a>', true);

            topLink.on('focus', function(e) {
                if (me.treeFocused === false) {
                    e.stopEvent();
                    var c = me.tree.getView().getSelectionModel().getCount();
                    if (c === 0)
                        me.selectNode(0);
                    else
                        me.setFocus();
                }
            });
        }, me);
        me.tree.on('afteritemexpand', me.onAfterItemExpand, me);
        me.tree.on('afteritemcollapse', me.onAfterItemCollapse, me);

        // если допускается фильтрация в грид - то вешаем обработчики событий
        if (me.filterToGrid) {
            me.on('beforefilterchange', me.onBeforeFilterChanged, me);
            me.grid.on('select', me.onRecordSelect, me);
            me.grid.on('deselect', me.onRecordDeselect, me);
            me.grid.on('beforeselect', me.beforeSelect, me);

            me.grid.on('afterrender', function(grid) {
                var el = grid.getView().el;
                el.on('focus', this.onGridFocus, this);
                el.on('blur', this.onGridBlur, this);

                var topLink = Ext.DomHelper.insertFirst(grid.el, '<a id="' + me.grid.id + '-top-link" class="doci-hidden-focus-link" href="#top2">top2</a>', true);

                topLink.on('focus', function(e) {
                    if (me.gridFocused === false) {
                        e.stopEvent();
                        var c = me.getGrid().getView().getSelectionModel().getCount();
                        if (c === 0) {
                            me.grid.getView().getSelectionModel().select(0);
                            me.setFocus();
                        }
                        else
                            me.setFocus();
                    }
                });

                //var el = grid.getView().el;
                el.on('keydown', function(evt) {
                    //Ctrl+Enter
                    if (evt.ctrlKey === true && evt.getKey() == Ext.EventObject.RETURN) {
                        evt.stopEvent();
                        var bId = me.up()._okBtnId;
                        var bOK = Ext.getCmp(bId);
                        if (bOK.isDisabled() === false) {
                            bOK.handler.call(bOK.scope || bOK, bOK);
                        }
                    }
                    //PgDn
                    if (evt.ctrlKey === true && evt.altKey === true && evt.getKey() == Ext.EventObject.PAGE_DOWN) {
                        evt.stopEvent();
                        var rb = me.down('toolbar[dock="bottom"] button[itemId="next"]');
                        //console.log(rb);
                        if (rb && !rb.disabled) {
                            var bBar = rb.up();
                            bBar.moveNext();
                            me.selectFirstRow();
                        }
                    }
                    //PgUp
                    if (evt.ctrlKey === true && evt.altKey === true && evt.getKey() == Ext.EventObject.PAGE_UP) {
                        evt.stopEvent();
                        var rb = me.down('toolbar[dock="bottom"] button[itemId="prev"]');
                        //console.log(rb);
                        if (rb && !rb.disabled) {
                            var bBar = rb.up();
                            bBar.movePrevious();
                            me.selectFirstRow();
                        }
                    }
                    //F5
                    if (evt.getKey() == Ext.EventObject.F5) {
                        evt.stopEvent();
                        var rb = me.down('toolbar[dock="bottom"] button[itemId="refresh"]');
                        if (rb) {
                            me.getGrid().store.reload();
                            me.selectFirstRow();
                        }
                    }
                });
            }, me);
        }
    },

    /**
     * Получение гридом фокуса
     * @private
     */
    onGridFocus: function(e, el) {
        this.gridFocused = true;
        this.removeCls('doci-gridpanel-lost-focus');
        this.addCls('doci-gridpanel-focus');

        var topLink = Ext.getDom(this.grid.getId() + '-top-link');
        if (topLink) {
            topLink.setAttribute('tabindex', -1);
        }
    },

    /**
     * Потеря фокуса у грида
     * @private
     */
    onGridBlur: function(e, el) {
        // задерживаем снятие флага фокусировки
        this.unfocusGridTask.delay(100);
        this.removeCls('doci-gridpanel-focus');
        this.addCls('doci-gridpanel-lost-focus');

        var topLink = Ext.getDom(this.grid.getId() + '-top-link');
        if (topLink) {
            topLink.removeAttribute('tabindex');
        }
    },

    /**
     * Получение гридом фокуса
     * @private
     */
    onTreeFocus: function(e, el) {
        this.treeFocused = true;
        this.removeCls('doci-gridpanel-lost-focus');
        this.addCls('doci-gridpanel-focus');

        var topLink = Ext.getDom(this.tree.getId() + '-top-link');
        if (topLink) {
            topLink.setAttribute('tabindex', -1);
        }
    },

    /**
     * Потеря фокуса у грида
     * @private
     */
    onTreeBlur: function(e, el) {
        // задерживаем снятие флага фокусировки
        this.unfocusTreeTask.delay(100);
        this.removeCls('doci-gridpanel-focus');
        this.addCls('doci-gridpanel-lost-focus');

        var topLink = Ext.getDom(this.tree.getId() + '-top-link');
        if (topLink) {
            topLink.removeAttribute('tabindex');
        }
    },

    /**
     * Визуальное отображение компоненту что хранилище загружается
     * @private
     */
    onTreeBeforeLoad: function() {
        this.setLoading();
    },

    /**
     * Убираем отображение о загрузке компонента
     * @private
     */
    onTreeAfterLoad: function() {
        this.setLoading(false);
    },

    /**
     * Функция возврата фокуса в панель
     */
    setFocus: function(interval) {
        var me = this,
            store = me.getTree().getStore(),
            root = store.getRootNode(),
            view = me.getTree().getView(),
            sModel = view.getSelectionModel(),
            hasSelection = sModel.hasSelection();

        interval = interval || 100;

        if (me.getLayout().getActiveItem && me.getLayout().getActiveItem().xtype == 'gridpanel') {
            me.setGridFocus(interval);
            return;
        }

        if (store.loading) {
            store.on('load', function() {
                me.setFocus();
            }, me, {single: true});
            return;
        }

        view.focus(false, true);

        // если дерево считает, что у нас есть выделение...
        if (hasSelection === true) {
            // считаем, что выделения нет и проверяем
            hasSelection = false;
            var r = sModel.getSelection()[0];
            // проверяем среди видимых узлов, есть ли искомый
            root.cascadeBy(function(node) {
                if (r.get('id') == node.get('id'))
                    hasSelection = true;
            });
        }

        // если выделенная запись не была найдена или ничео и не было выделено
        if (hasSelection === false) {
            // если у нас отображается корень - то выделяем его
            if (me.getTree().rootVisible)
                sModel.select(root);
            else if (root.childNodes.length > 0) // иначе - первого потомка, если он есть
                sModel.select(root.childNodes[0]);
        }

        // Обновит после полной периресовки и подсветить
        // Анимированно подсвечивает запись в табл
        var task = Ext.TaskManager.start({
            run: function(n) {
                if (n < 2) return;
                Ext.TaskManager.stop(task);//удаляем задачу
                var view = this.getTree().getView();
                var row = view.getSelectedNodes()[0];
                if (row) {
                    view.onRowSelect(row);
                    view.focusRow(row);
                }
            },
            interval: interval,
            scope: this
        });

        me.callParent([]);
    },

    /**
     * Возврат фокуса в линейный список, если он активен
     * @private
     */
    setGridFocus: function(interval) {
        var me = this,
            view = me.getGrid().getView(),
            store = me.getGrid().getStore(),
            sModel = view.getSelectionModel();

        //если стор не загружен, то откладуваем установку фокуса
        //до окончательной загрузки
        if (store.loading) {
            store.on('load', function() {
                this.setGridFocus(interval);
            }, this);
        }

        //Если нет выделеной записи, выделяем т.к. устанавливаем фокус
        if (!sModel.hasSelection() && store.getCount() > 0) {
            sModel.select(0);
        }

        view.focus(false, true);
        //Обновит после полной периресовки и подсветить
        var task = null;
        //Анимированно подсвечивает запись в табл
        task = Ext.TaskManager.start({
            run: function(n) {
                if (n < 2) return;
                Ext.TaskManager.stop(task);
                var view = this.getGrid().getView();

                var row = view.getSelectedNodes()[0];
                if (row) {
                    view.onRowSelect(row);
                    view.focusRow(row);
                }
            },
            interval: interval,
            scope: this
        });
    },

    /**
     * Выделить запись в дереве и установить фокус
     * @param {Ext.data.Model} record запись
     * @param {Boolean} isfocused флаг автоматической установки фокуса на запись
     */
    select: function(record, isfocused) {
        var me = this,
            view = me.getTree().getView(),
            sModel = view.getSelectionModel();

        sModel.deselectAll();
        sModel.select(record);
        me.callParent([record, isfocused]);
    },

    /**
     * Снятие выделения со всех записей
     */
    deselect: function() {
        this.getTree().getView().getSelectionModel().deselectAll();
    },

    /**
     * Подмена компонент, если используется фильтрация в линейный список.
     * @private
     * @param  {DOCI.view.Tree/DOCI.view.Grid} item Компонент, который необходимо активировать
     */
    setActiveItem: function(item) {
        Ext.suspendLayouts();
        this.getLayout().setActiveItem(item);
        Ext.resumeLayouts(true);
    },

    /**
     * Обработка события изменения фильтра. Выполняет подмену компонента при `filterToGrid = true`.
     * @private
     * @param  {DOCI.view.Tree} cmp Исходный компонент
     * @param  {Object} filter Новые параметры фильтрации
     */
    onBeforeFilterChanged: function(cmp, filter) {
        var me = this,
            reload_button = this.buttonCmpGet('reload');

        me.onRecordDeselect();

        if (DOCI.isEmptyObject(filter)) {
            if (reload_button) reload_button.show();
            me.setActiveItem(0);
            me.filter = filter;
            me.fireEvent('filterchange', me, filter, filter);
            return false;
        } else {
            if (reload_button) reload_button.hide();
            me.setActiveItem(1);
            return true;
        }
    },


    // @override
    // применение фильтра к стору
    applyFilter: function(filter, autoSelect) {
        var me = this;
        var removeFilter_button = this.buttonCmpGet('removeFilter'),
            store = this.store,
            store_filters = [];

        // если используется фильтрация в таблицу
        if (this.filterToGrid) store = this._intStore;


        if (removeFilter_button) {
            if (this.compareFilter(filter, {})) {
                removeFilter_button.setDisabled(true);
            } else {
                removeFilter_button.setDisabled(false);
            }
        }

        // добавляем экстра-фильтр
        if (this.extraFilter) {
            var merge = Ext.clone(this.extraFilter);
            Ext.applyIf(merge, filter);
            filter = merge;
        }

        // если используется фильтрация в таблицу
        if (this.filterToGrid) {
            // создаем объекты фильтра и применяем к стору
            Ext.Object.each(filter, function(k, v) {
                store_filters.push(
                    Ext.create('Ext.util.Filter', {property: k, value: v})
                );
            });

            //если onlyLeaf == true, то в отфильтрованном гриде будут только листья дерева
            //(добавится фильтр is_leaf)
            if (this.onlyLeaf === true) {
                store_filters.push(Ext.create('Ext.util.Filter', {property: this.onlyLeafFilterName, value: true}));
            }

            store.clearFilter(true);
            //Для выборки всех записей
            // if(store_filters[1] && store_filters[1].property=='leaf' && this.sel_leaf == true){
            // 	store_filters[1].value=false;
            // };
            store.filter(store_filters);

            // подвешиваем событие на загрузку стора для выделения записи, если разрешено
            if (autoSelect === undefined || autoSelect === true) {
                store.on('load', function(store, records, successful) {
                    me.setSearchFocus();
                }, this, {single: true, delay: 100});
            }

            // иначе - фильтруем непосредственно дерево
        } else {
            // создаем объекты фильтра и добавляем их к стору
            store.filters.clear();
            Ext.Object.each(filter, function(k, v) {
                store.filters.add(Ext.create('Ext.util.Filter', {property: k, value: v}));
            });
            // сбрасываем дерево и перезагружаем корень
            var root = this.tree.getRootNode();
            root.set('loaded', false);
            store.load({node: root,
                callback: function(records, operation, success) {
                    if (success) root.expand();
                    me.setSearchFocus();
                }
            });
        }
    },

    /**
     * ф-ия отработает после удаления записи из стора
     */
    afterRemoveItem: function() {
        this.onRecordDeselect();
    },

    /**
     * ф-ия отработает после добавления записи в стор
     */
    afterAddItem: function() {
        this.onRecordDeselect();
    },

    /**
     * Получить компонент дерева.
     * @return {Ext.tree.Panel}
     */
    getTree: function() {
        return this.tree;
    },

    /**
     * Получить компонент линейного списка.
     * @return {Ext.grid.Panel}
     */
    getGrid: function() {
        return this.grid;
    },

    /**
     * Выделить запись в дереве.
     * @param {Ext.data.Model} node запись.
     */
    selectNode: function(node) {
        this.select(node);
    },

    /**
     * Поиск записи в дереве.
     * @param  {String} param Параметр для поиска.
     * @param  {String} value Значение параметра.
     * @params {Ext.data.NodeInterface} Объект узла дерева, начиная с которого производим поиск.
     * @return {Ext.data.NodeInterface} Объект узла дерева или NULL, если ничего не найдено.
     */
    findNode: function(param, value, startNode) {
        if (Ext.isEmpty(startNode))
            startNode = this.tree.getRootNode();

        return startNode.findChild(param, value, true);
    },

    /**
     * Инициализация и создание панели дерева
     *
     * @return {Ext.tree.Panel}
     */
    createTree: function() {
        var me = this;
        me.defaultTree = me.defaultTree || {};
        Ext.apply(me.defaultTree, {
            viewConfig: {
                toggleOnDblClick: false
            },
            store: me.store,
            listeners: {
                afterrender: function() {
                    this.el.addKeyMap({
                        eventName: "keydown",
                        binding: [
                            {
                                key: Ext.EventObject.ENTER,
                                fn: function() {
                                    me.onEditButton();
                                }
                            },
                            //Удаление
                            {
                                ctrl: me.bTip.delRecord.ctrl,
                                alt: me.bTip.delRecord.alt,
                                shift: me.bTip.delRecord.shift,
                                key: me.bTip.delRecord.key,
                                fn: function() {
                                    var b = me.buttonCmpGet('remove');
                                    if (b && b.items[0].disabled === false) me.onRemoveButton();
                                }
                            }
                        ]
                    });
                }
            }
        });

        var tree = Ext.create('Ext.tree.Panel', me.defaultTree);

        return tree;
    },

    /**
     * Инициализация и создание панели грида
     *
     * @return {Ext.grid.Panel}
     */
    createGrid: function() {
        var me = this;
        me.defaultGrid = me.defaultGrid || {};

        // копируем столбцы из дерева и заменяем xtype с treecolumn на gridcolumn, если уже не указаны столбцы вручную
        if (me.defaultTree.columns.length > 0 && me.defaultGrid.columns.length === 0) {
            me.defaultGrid.columns = Ext.clone(me.defaultTree.columns);
            for (var i in me.defaultGrid.columns) {
                if (me.defaultGrid.columns[i].xtype == 'treecolumn') {
                    me.defaultGrid.columns[i].xtype = 'gridcolumn';
                    break;
                }
            }
        }

        if (me.defaultGrid.pager === true) {
            var pagerParams = me.defaultGrid.pagerParams || {};
            Ext.applyIf(pagerParams, {
                store: me._intStore,
                displayInfo: true
            });
            me.defaultGrid.bbar = Ext.create('Ext.PagingToolbar', pagerParams);
        }

        Ext.apply(me.defaultGrid, {
            store: me._intStore,
            listeners: {
                scope: me,
                itemdblclick: me.onItemDblClick,
            }
        });

        var grid = Ext.create('Ext.grid.Panel', me.defaultGrid);

        return grid;
    },

    /**
     * Выполнен двойной щелчок на записи
     * @param {Ext.grid.View} view Компонент отвечающий за отображение записей грида
     * @param {Ext.data.Model} record Запись грида
     * @param {HTMLElement} item  HTML еlement
     * @param {Number} index  Номер в гриде
     */
    onItemDblClick: function(view, record, item, index, event, eOpts) {
        if (this.buttonCmpGet('edit'))
            this.fireEvent('editbuttonclick', this, [record]);
    },


    onItemCollapse: function(record) {
        this.setFocus();
    },

    /**
     * Ф-ия вызывается при каждом выборе записи и активирует кнопки редактирования и удаления
     * @param {Ext.selection.RowModel} selectModel Модель выбора
     * @param {Ext.data.Model} record Выбранная запись
     * @param {Number} index  Индекс элемента
     */
    onRecordSelect: function(rowModel, record, index, eOpts) {
        this.callParent([rowModel.selected.items]);
    },

    //Нажата кнопка созддания
    onCreateButton: function() {
        var r = this.tree.getSelectionModel().getSelection();
        this.fireEvent('createbuttonclick', this, r);
    },

    //Нажата кнопка редактирования
    onEditButton: function() {
        var records = this.tree.getSelectionModel().getSelection();
        this.fireEvent('editbuttonclick', this, records);
    },

    //Нажата кнопка Удаления
    onRemoveButton: function() {
        var records = this.tree.getSelectionModel().getSelection();
        this.fireEvent('removebuttonclick', this, records);
    },


    /* Создание кнопок панели */

    // кнопка создания записи
    reloadButton: function() {
        var me = this;

        var action = Ext.create('Ext.Action', {
            text: me.txtRefresh,
            iconCls: 'doci-icon-refresh-16',
            handler: me.onReloadButton,
            tooltip: me.txtRefresh + me.hKey.render(me.bTip.refreshTab),
            scope: me
        });
        me._tbarObjects['reload'] = action;
        me._contextMenu.push(action);

        return Ext.create('Ext.Button', action).setText('');
    },


    onReloadButton: function(but, evt) {
        var me = this,
            root = me.tree.getRootNode(),
            b = Ext.Element.getActiveElement().type;

        me.fireEvent('reloadbuttonclick', me);
        root.set('loaded', false);
        me.store.load({
            node: root,
            callback: function(records, operation, success) {
                if (success) {
                    root.expand();
                    me.deselect();
                    me.setFocus();
                    var rb = me.down('toolbar[dock="top"] button[tag="reload"]');
                    if (b == 'button') rb.focus('', 50);
                }
            }
        });
    },


    //Снятие галок со всех чекбоксов
    uncheckAll: function() {
        var me = this;
        me.getTree().getRootNode().cascadeBy(function() {
            this.set('checked', false);
        });
    },


    onAfterItemCollapse: function(node, index, item) {
        var cls = 'x-grid-tree-node-expanded';
        var it = Ext.get(item);
        it.removeCls(cls);
    },


    onAfterItemExpand: function(node, index, item) {
        var cls = 'x-grid-tree-node-expanded';
        var it = Ext.get(item);
        it.addCls(cls);
    },


    //#1032
    setSearchFocus: function() {
        var me = this,
            activeCmp = this.getLayout().getActiveItem(),
            view = activeCmp.getView(),
            store = activeCmp.getStore(),
            sModel = view.getSelectionModel(),
            searchCmp = me.buttonCmpGet('search');

        //если стор не загружен, то откладуваем установку фокуса
        //до окончательной загрузки
        if (store.loading) {
            store.on('load', function() {
                me.setSearchFocus();
            }, me);
        }

        //Если нет выделеной записи, выделяем т.к. устанавливаем фокус
        if (!sModel.hasSelection() && store.getCount() > 0 && me.selRow)
            sModel.select(0);

        // если есть поле быстрого поиска - ставим фокус в него
        if (!Ext.isEmpty(searchCmp))
            searchCmp.focus('', 50);
    }
});
