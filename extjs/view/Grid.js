/**
 * @class DOCI.view.Grid
 * @extend DOCI.view.Base
 *
 * Компонент позволяющий организовать работу с линейными списками
 *
 * Пример:
 *
 *     var store = Ext.create('DOCI.data.Store',{
 *         ...
 *     });
 *
 *     Ext.create('DOCI.view.Grid',{
 *         tbarButtons : ['->','search'],
 *         searchField : 'fam_in',
 *         searchText : 'Фамилия',
 *         defaultGrid : {
 *             pager : true,
 *             columns : [
 *                 {
 *                     text : 'Фамилия',
 *                     dataIndex : 'fam'
 *                 },{
 *                     text : 'Имя',
 *                     dataIndex : 'im'
 *                 },{
 *                     text : 'Отчество',
 *                     dataIndex: 'ot'
 *                 },
 *             ]
 *         }
 *     });
 *
 * @author Еременко В.А.
 */
Ext.define('DOCI.view.Grid', {
    extend: 'DOCI.view.Base',
    alias: 'widget.basegrid',

    /**
     * Выделенная запись в списке.
     * Применяется для установки автоматического фокуса на указанную запись
     *
     * @type {Ext.data.Model}
     */
    record: null,

    /**
     * Хранилище записей
     *
     * @type {DOCI.data.Store}
     */
    store: null,

    /**
     * Объект настроек грида (колонки,пейджер и.т.д) см. {@link Ext.grid.Panel}
     * Пример:
     *
     *     defaultGrid: {
	 *         pager: true,
	 *         columns: [
	 *             {
	 *                 xtype: 'checkcolumn',
	 *                 header: ' ',
	 *                 dataIndex: 'on_schedule',
	 *                 width: 30,
	 *                 resizable: false,
	 *                 menuDisabled: true,
	 *                 hideable: false,
	 *                 editor: {
	 *                     xtype: 'checkbox',
	 *                     cls: 'x-grid-checkheader-editor'
	 *                 },
	 *                 listeners: {
	 *                     'checkchange': me.onChangeRecord,
	 *                     scope: me
	 *                 }
	 *             },{
	 *                 text: 'Фамилия',
	 *                 flex: 1,
	 *                 dataIndex: 'fam'
	 *             },{
	 *                 text: 'Имя',
	 *                 flex: 1,
	 *                 dataIndex: 'im'
	 *             },{
	 *                 text: 'Отчество',
	 *                 flex: 1,
	 *                 dataIndex: 'ot'
	 *             }
	 *         ]
	 *     }
     *
     * @type {Object}
     */
    defaultGrid: undefined, // конфигурация таблицы


    initComponent: function() {
        var me = this;

        me.hKey = DOCI.HotKeys;
        me.bTip = me.hKey.treeGridKeys;

        Ext.define('Ext.PagingToolbarTip', {
            override: 'Ext.PagingToolbar',
            prevText: me.txtPrevPage + me.hKey.render(me.bTip.goPgUp),
            nextText: me.txtNextPage + me.hKey.render(me.bTip.goPgDn),
            refreshText: me.txtRefresh + me.hKey.render(me.bTip.refreshTab),
        });

        me.tag = 'Grid';
        // пустой объект конфигурации грида
        Ext.applyIf(me, {
            defaultGrid: {},
            gridFocused: false,
            // отложенное задание сброса флага фокуса
            unfocusGridTask: new Ext.util.DelayedTask(function() {
                me.gridFocused = false;
            })
        });

        // задание начальных значений таблицы
        Ext.applyIf(me.defaultGrid, {
            multiSelect: false,
            border: false,
            pager: false,
            columns: []
        });

        me.grid = me.createGrid();

        me.items = [ me.grid ];

        me.callParent(arguments);

        //me.relayEvents(me.tree,['select','deselect']);
        me.grid.on('select', me.onRecordSelect, me);
        me.grid.on('deselect', me.onRecordDeselect, me);
        me.grid.on('beforeselect', me.beforeSelect, me);

        me.grid.on('afterrender', function(grid) {
            me.createTopLink(grid, me);
        }, me);
    },


    createTopLink: function(grid, me) {
        var el = grid.getView().el;

        el.on('focus', this.onGridFocus, this);
        el.on('blur', this.onGridBlur, this);

        var topLink = Ext.DomHelper.insertFirst(grid.el, '<a id="' + grid.getId() + '-top-link" class="doci-hidden-focus-link" href="#top">top</a>', true);

        topLink.on('focus', function(e) {

            if (me.gridFocused === false) {
                e.stopEvent();
                var c = me.getGrid().getView().getSelectionModel().getCount();
                if (c === 0) {
                    me.select(0, true);
                    me.setFocus();
                }
                else
                    me.setFocus();
            }
        });
    },

    /**
     * Получение гридом фокуса
     * @private
     */
    onGridFocus: function(e, el) {
        this.gridFocused = true;
        this.removeCls('doci-gridpanel-lost-focus');
        this.addCls('doci-gridpanel-focus');

        var topLink = Ext.getDom(this.grid.getId() + '-top-link');
        if (topLink) {
            topLink.setAttribute('tabindex', -1);
        }
    },

    /**
     * Потеря фокуса у грида
     * @private
     */
    onGridBlur: function(e, el) {
        // задерживаем снятие флага фокусировки
        this.unfocusGridTask.delay(100);

        this.removeCls('doci-gridpanel-focus');
        this.addCls('doci-gridpanel-lost-focus');

        var topLink = Ext.getDom(this.grid.getId() + '-top-link');
        if (topLink) {
            topLink.removeAttribute('tabindex');
        }
    },

    /**
     * Повторное выделение записей в таблице
     */
    reselect: function() {
        var me = this,
            view = me.getGrid().getView(),
            sModel = view.getSelectionModel(),
            selection = sModel.getSelection();

        if (!Ext.isEmpty(selection) && selection.length > 0) {
            me.select(selection);
        }
    },

    /**
     * Выделить запись в гриде и установить фокус
     * @param {Ext.data.Model|number} record запись или индекс
     * @param {Boolean} isfocused флаг автоматической установки фокуса на запись
     */
    select: function(record, isfocused) {
        var me = this,
            view = me.getGrid().getView(),
            sModel = view.getSelectionModel();

        if (me.store.count() > 0) {
            sModel.select(record);
            me.callParent([record, isfocused]);
        }
    },

    /**
     * Снятие выделения со всех записей
     */
    deselect: function() {
        this.getGrid().getView().getSelectionModel().deselectAll();
    },

    /**
     * Функция возврата фокуса в панель
     * @param {Number} interval Задержка перед тем как установить фокус (по умолчанию = 100 мс)
     */
    setFocus: function(interval) {
        var me = this,
            view = me.getGrid().getView(),
            store = me.getGrid().getStore(),
            sModel = view.getSelectionModel();

        interval = interval || 100;

        //если стор не загружен, то откладуваем установку фокуса
        //до окончательной загрузки
        if (store.loading) {
            store.on('load', function() {
                this.setFocus();
            }, this);
        }

        //Если нет выделеной записи, выделяем т.к. устанавливаем фокус
        if (!sModel.hasSelection() && store.getCount() > 0) {
            sModel.select(0);
        }

        view.focus(false, true);
        //Обновит после полной периресовки и подсветить
        var task = null;
        //Анимированно подсвечивает запись в табл
        task = Ext.TaskManager.start({
            run: function(n) {
                if (n < 2) return;
                Ext.TaskManager.stop(task);//удаляем задачу
                var view = this.getGrid().getView();

                var row = view.getSelectedNodes()[0];
                if (row) {
                    view.onRowSelect(row);
                    view.focusRow(row);
                }
            },
            interval: interval,
            scope: this
        });
        //}

        me.callParent([]);
    },

    /**
     * Поиск записи в собственном хранилище
     * @param  {String} field Название поля по которому производится поиск
     * @param  {String/RegExp} value Искомое значение
     * @return {Ext.data.Model} Запись с данными или null
     */
    find: function(field, value) {
        var me = this,
            idx, record = null;

        idx = me.store.find(field, value);
        if (idx >= 0)
            record = me.store.getAt(idx);

        return record;
    },

    /**
     * ф-ия отработает после удаления записи из стора
     */
    afterRemoveItem: function(store, model, records) {
        var grid = this.getGrid();
        var cnt = grid.store.getCount();
        var smodel = grid.getSelectionModel();
        var nr = records;
        if (cnt > 0) {
            if (nr > cnt - 1) nr = cnt - 1;
            smodel.select(nr);
        } else {
            this.onRecordDeselect();
        }
    },

    /**
     * ф-ия отработает после добавления записи в стор
     * @param {DOCI.data.Store} store стор
     * @param {Ext.data.Model[]} records добавляемые записи
     */
    afterAddItem: function(store, records) {

        var grid = this.getGrid();
        var model = grid.getSelectionModel();
        model.select(records);
    },

    /**
     * Получить грид
     * @return {Ext.grid.Panel}
     */
    getGrid: function() {
        return this.grid;
    },

    /**
     * Инициализация и создание панели грида
     *
     * @return {Ext.grid.Panel}
     */
    createGrid: function() {
        var me = this;
        me.defaultGrid = me.defaultGrid || {};

        if (me.defaultGrid.pager === true) {
            var pagerParams = me.defaultGrid.pagerParams || {};
            Ext.applyIf(pagerParams, {
                store: me.store,
                displayInfo: true
            });
            me.defaultGrid.bbar = Ext.create('Ext.PagingToolbar', pagerParams);
            me.defaultGrid.bbar.on('beforechange', function() {
                me.store.on('load', function() {
                    me.setFocus();
                }, null, {single: true});
            });
        }

        Ext.apply(me.defaultGrid, {
            store: me.store,
            listeners: {
                afterrender: function() {
                    me.onRecordDeselect();
                },
                itemcontextmenu: function(view, rec, node, index, e) {
                    if (me.getContextMenu() !== null && me.getContextMenu().length !== 0) {
                        e.stopEvent();
                        me.getContextMenu().showAt(e.getXY());
                        return false;
                    }
                },
                scope: me
            }
        });

        var grid = Ext.create('Ext.grid.Panel', me.defaultGrid);
        grid.on('itemdblclick', me.onItemDblClick, me);

        return grid;
    },

    /**
     * Выполнен двойной щелчок на записи
     * @param {Ext.grid.View} view Компонент отвечающий за отображение записей грида
     * @param {Ext.data.Model} record Запись грида
     */
    onItemDblClick: function(view, record) {
        if (this.buttonCmpGet('edit'))
            this.fireEvent('editbuttonclick', this, [record]);
    },

    /**
     * Нажата кнопка редактирования
     */
    onEditButton: function() {
        var records = this.getGrid().getSelectionModel().getSelection();
        this.fireEvent('editbuttonclick', this, records);
    },

    /**
     * нажата кнопка удаления
     */
    onRemoveButton: function() {
        var records = this.getGrid().getSelectionModel().getSelection();
        this.fireEvent('removebuttonclick', this, records);
    },

    /**
     * Ф-ия вызывается при каждом выборе записи и активирует кнопки редактирования и удаления
     * @param {Ext.selection.RowModel} selectModel Модель выбора
     * @param {Ext.data.Model} record Выбранная запись
     */
    onRecordSelect: function(selectModel, record) {
        if (this.defaultGrid.multiSelect)
            this.callParent([selectModel.getSelection()]);
        else
            this.callParent([record]);
    },


    onRecordDeselect: function() {
        var edit_button = this.buttonCmpGet('edit'),
            remove_button = this.buttonCmpGet('remove'),
            grid = this.getGrid(),
            selCount = grid.getSelectionModel().getCount();

        if (selCount < 1) {
            if (edit_button) edit_button.setDisabled(true);
            if (remove_button) remove_button.setDisabled(true);
        }

        this.fireEvent('deselect', this);
    },


    //#1032
    setSearchFocus: function() {
        var me = this,
            view = me.getGrid().getView(),
            store = me.getGrid().getStore(),
            sModel = view.getSelectionModel(),
            searchCmp = me.buttonCmpGet('search');

        //если стор не загружен, то откладуваем установку фокуса
        //до окончательной загрузки
        if (store.loading) {
            store.on('load', function() {
                me.setSearchFocus();
            }, me);
        }

        //Если нет выделеной записи, выделяем т.к. устанавливаем фокус
        if (!sModel.hasSelection() && store.getCount() > 0 && me.selRow)
            sModel.select(0);

        // если есть поле быстрого поиска - ставим фокус в него
        if (!Ext.isEmpty(searchCmp))
            searchCmp.focus('', 50);
    }
});
