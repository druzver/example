<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 13:39
 */

namespace REST;
use NewsApi\DOCI\REST;


/**
 * Абстрактный класс API
 *
 *
 * @package REST
 */
abstract class AbstractAPI {


	/**
	 * @var \NewsApi\DOCI\REST
	 */
	private $api;



	/**
	 * Конструктор класса
	 *
	 * @param \NewsApi\DOCI\REST $api объект АПИ
	 */
	function __construct( REST $api){
		$this->api = $api;
	}


	/**
	 * Возвращает объект по работе с запросами
	 *
	 * @return Browser
	 */
	public function getBrowser()
	{
		return $this->api->getBrowser();
	}

	/**
	 * @return \NewsApi\DOCI\REST
	 */
	public function getApi()
	{
		return $this->api;
	}


} 