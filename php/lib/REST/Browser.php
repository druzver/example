<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 11:54
 */

namespace REST;


/**
 * Использует в качестве библиотеки запростов
 * sudo apt-get install curl libcurl3 libcurl3-dev php5-curl php5-mcrypt
 *
 * Класс браузера REST запросов
 * @package REST
 */
class Browser {

	private $curl=null;

	function __construct() {

		$this->useragent = 'PHP-RESTBrowser ('.$_SERVER['SERVER_NAME'].')';
		$this->cookieFile = tempnam('','cookie');
	}


	/**
	 * Отчистка куков
	 *
	 * @throws \Exception
	 */
	function cleanCookie() {
		$this->cookieFile = tempnam('','cookie');
	}

	protected $requestsLog=array();

	/**
	 * Возвращает лог всех запросов
	 *
	*/
	public function getRequestsLog(){
		return $this->requestsLog;
	}

	/**
	 * Выполнение запроса
	 * @throws
	 * @param Request $request
	 * @return Response
	 */
	public function exequteQuery( Request $request ) {



		//Тип запроса (POST, PUT, GET, DELETE)
		$method = $request->getMethod();
		$response = null;

		//Определяем алгоритм формирования запроса
		switch($method) {
			case 'POST':
					$response  = $this->post($request);
				break;

			case 'GET':
				$response  = $this->get($request);
				break;

			case 'PUT':
				$response  = $this->put($request);
				break;

			case 'DELETE':
				$response  = $this->delete($request);
				break;

			default:
				throw new \Exception('Not support query METHOD'.$method);
		}

		$this->requestsLog[] = $response;

		//print_r($response);

		return $response;
	}

	private $cookieFile = null;
	private $useragent = 'PHP VIRESTBot ';

	/**
	 * Алгоритм формирования GET запроса
	 *
	 * @param Request $request
	 * @return Response
	 */
	private function get(Request $request) {
		$curl = curl_init();
		//Проверка, открыта ли сессия
		if(!$curl) {
			throw new \Exception('Curl session not started');
		}

		//Устанавливаем параметры запроса
		curl_setopt($curl, CURLOPT_URL, $request->getUrl() );
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);

		curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookieFile );
		curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookieFile );
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $request->getAutoRedirect() );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($curl, CURLOPT_USERAGENT, $this->useragent);

		//Выполнение запроса
		$content = curl_exec($curl);

		//Формирование ответа
		$response = Response::parseFromString($content, $request);


		curl_close($curl);

		return $response;
	}

	/**
	 * Алгоритм формирования POST запроса
	 *
	 * @param Request $request
	 * @return Response
	 */
	private function post(Request $request) {

		$curl = curl_init();
		//Проверка, открыта ли сессия
		if(!$curl) {
			throw new \Exception('Curl session not started');
		}

		$data = $request->getParams();
		$files = $request->getFiles();
		$data = array_merge($data, $files);



		if(count($files) == 0) {
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
		} else {
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		}


		curl_setopt($curl, CURLOPT_URL, $request->getUrl() );
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $request->getMethod() );




		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $request->getAutoRedirect() );

		curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookieFile );
		curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookieFile );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($curl, CURLOPT_USERAGENT, $this->useragent);

		curl_setopt($curl, CURLOPT_HEADER, true);

		//Выполнение запроса
		$content = curl_exec($curl);

		//информация о запросе (время выполнения, статус ответа, кол-во редиректов и т.д.)
		$requestInfo = curl_getinfo ($curl);

		//Формирование ответа
		$response = Response::parseFromString($content, $request, $requestInfo);

		curl_close($curl);

		//print_r($response);

		return $response;
	}


	public function sendFile(Request $request) {

		$curl = curl_init();
		//Проверка, открыта ли сессия
		if(!$curl) {
			throw new \Exception('Curl session not started');
		}

		$data = $request->getParams();
		$files = $request->getFiles();
		$data = array_merge($data, $files);


		//curl_setopt($curl, CURLOPT_UPLOAD, 1);
		/*curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			//"Content-Type:multipart/form-data",
			//"Content-Type:application/x-www-form-urlencoded"
		));*/

		curl_setopt($curl, CURLOPT_HEADER, 1);
		curl_setopt($curl, CURLOPT_VERBOSE, 1);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_USERAGENT, $this->useragent);
		curl_setopt($curl, CURLOPT_URL,  $request->getUrl() );
		//curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $request->getMethod() );
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookieFile );
		curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookieFile );
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);

		//------------


//		curl_setopt($curl, CURLOPT_URL, $request->getUrl() );
//		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $request->getMethod() );
//		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
//		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//
//		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $request->getAutoRedirect() );
//
//		curl_setopt($curl, CURLOPT_COOKIEFILE, $this->cookieFile );
//		curl_setopt($curl, CURLOPT_COOKIEJAR, $this->cookieFile );
//		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
//		curl_setopt($curl, CURLOPT_USERAGENT, $this->useragent);
//
//		curl_setopt($curl, CURLOPT_HEADER, true);

		//Выполнение запроса
		$content = curl_exec($curl);

		//информация о запросе (время выполнения, статус ответа, кол-во редиректов и т.д.)
		$requestInfo = curl_getinfo ($curl);

		//Формирование ответа
		$response = Response::parseFromString($content, $request, $requestInfo);

		curl_close($curl);

		return $response;
	}

	/**
	 * Алгоритм формирования PUT запроса
	 *
	 * @param Request $request
	 * @return Response
	 */
	private function put(Request $request) {
		return $this->post($request);
	}

	/**
	 * Алгоритм формирования DELETE запроса
	 * @param Request $request
	 * @return Response
	 */
	private function delete(Request $request) {
		return $this->post($request);
	}





} 