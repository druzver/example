<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 12:55
 */

namespace REST;


/**
 * Class Response
 * @package REST
 */
class Response {

	/**
	 * Текст ответа
	 * @var string
	 */
	private $sourceResponse=null;

	/**
	 * @return string
	 */
	public function getSourceResponse()
	{
		return $this->sourceResponse;
	}

	/**
	 * Объект запроса
	 * @var Request
	 */
	private $request;


	/**
	 * @var array
	 */
	private $headers=array();


	/**
	 * тело ответа
	 * @var string
	 */
	private $body = null;



	/**
	 * @return Request
	 */
	public function getRequest()
	{
		return $this->request;
	}

	/**
	 * @return string
	 */
	public function getBody()
	{
		return $this->body;
	}

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->headers;
	}

	/**
	 * Статистика запроса
	 * @var array
	 */
	private $info = array();

	/**
	 * Возвращает статистику запроса
	 * @return array
	 */
	public function getInfo()
	{
		return $this->info;
	}



	/**
	 * Создает объект овета, разбирая строку ответа
	 *
	 * @param string $str
	 * @param Request $request
	 * @param array $requestInfo
	 * @return Response
	 */
	public static function parseFromString($str, Request $request, $requestInfo = array()) {
		$response = new Response();

		$response->sourceResponse = $str;
		$response->info = $requestInfo;

		//Разбиваем ответ на заголовки и тело ответа
		$data = explode("\r\n\r\n",$str);

		//текстовоя часть с телом ответа
		$response->body = array_pop ($data);//Достаем последний элемент массива
		//текстовоя часть с заголовками
		$headerPart= array_pop ($data);
		$response->headers = $response->parseHeadersFromString($headerPart);

		//ссылка на объект запроса
		$response->request  = $request;

		return $response;
	}


	/**
	 * Парсинг строки с заголовками ответа
	 *
	 * @param $header_text строка содержащая заголовки ответа
	 * @return array
	 */
	private function parseHeadersFromString($header_text) {
		$headers = array();
		foreach (explode("\r\n", $header_text) as $i => $line)
			if ($i === 0)
				$headers['http_code'] = $line;
			else
			{
				list ($key, $value) = explode(': ', $line);

				$headers[$key] = $value;
			}

		return $headers;
	}


	/**
	 * Возвращает статус ответа
	 * @return string
	 */
	function getStatus() {

		// имеет формат  'HTTP/1.1 406 Not Acceptable'
		return $this->info['http_code'];
//		$dirtyStr = $this->getHeader('http_code');
//		$data = explode(' ', $dirtyStr);
//		return $data[1];
		//return '404';
	}


	/**
	 * Сравнивает статус ответа с указанным значением
	 * @param integer $status
	 * @return bool
	 */
	function isStatus($status) {

		return ( (int)$status === (int)$this->getStatus());
	}


	/**
	 * Получить заголовок по ключу
	 * @param string $key
	 * @return null|string
	 */
	function getHeader($key) {
		return @$this->headers[$key];
	}

}