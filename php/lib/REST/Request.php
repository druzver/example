<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 12:05
 */

namespace REST;


class Request {

	/**
	 *
	 * @var string null
	 *
	 */
	private $url=null;

	private $method='GET';

	private $headers=array();

	private $files = array();

	private $params = array();

	private $autoRedirect = false;

	/**
	 * авторедирект запросов
	 * @param boolean $autoRedirect
	 */
	public function setAutoRedirect($autoRedirect)
	{
		$this->autoRedirect = $autoRedirect;
	}

	/**
	 * авторедирект запросов
	 * @return boolean
	 */
	public function getAutoRedirect()
	{
		return $this->autoRedirect;
	}


	/**
	 * @param array $params
	 */
	public function setParams($params)
	{
		$this->params = $params;
	}

	/**
	 * @return array
	 */
	public function getParams()
	{
		return $this->params;
	}

	/**
	 * @param string $method
	 */
	public function setMethod($method)
	{
		$this->method = $method;
	}

	/**
	 * @return string
	 */
	public function getMethod()
	{
		return $this->method;
	}

	/**
	 * @param array $headers
	 */
	public function setHeaders($headers)
	{
		$this->headers = $headers;
	}

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->headers;
	}

	/**
	 * @param array $files
	 */
	public function setFiles($files)
	{
		$this->files = $files;
	}

	/**
	 * @return array
	 */
	public function getFiles()
	{
		return $this->files;
	}

	/**
	 * @param null $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @return null
	 */
	public function getUrl()
	{
		return $this->url;
	}




} 