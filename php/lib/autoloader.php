<?php


/**
 * Ф-ия подключения классов относительно текущей директории
 * @param $className
 */
function newsshare_autoloader($className) {

	$clsPath = str_replace("\\", '/', $className);
	$file = dirname(__FILE__) . '/' . $clsPath . ".php";

	if(file_exists($file)){
		include_once($file);
	}
}

//регистрация ф-ии
spl_autoload_register('newsshare_autoloader');



