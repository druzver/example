<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 11.06.14
 * Time: 14:43
 */

namespace NewsApi\DOCI\API;

use NewsApi\Filter;
use REST\AbstractAPI;
use REST\Request;
use REST\Response;

class TranslateNews extends  AbstractAPI {

	public $host = 'rga';
	public $lang = 'ru';
	public $map = '02.07.06';
	public $iface = 'restapi';
	public $cmd = 'news';

	/**
	 * Формирует болванку(стандартную заготовку) запроса
	 *
	 * @param Filter $filter
	 * @return  Request
	 */
	private function createRequest(Filter $filter=null) {
		$baseApi = $this->getApi();
		$request = new Request();

		//фильтр (параметры) для url
		$filters = array();
		if($filter) {
			$filters = $filter->getFilters();
		}

		//Формируем УРЛ для запроса.
		$url = $baseApi->constructUrl($this->host,
			$this->lang,
			$this->map,
			$this->iface,
			$this->cmd,
			$filters);

		$request->setUrl($url);
		//передаем параметры фильтрации
		return $request;
	}


	/**
	 * Преобразует ответ в структуру
	 * Ошибка:
	 *  {
	 *      status: false
	 *      message: 'some message'
	 *  }
	 *
	 * Все ок:
	 *  {
	 *      status: true
	 *      data: [...]
	 *  }
	 *
	 * @param Response $response
	 * @return array|mixed
	 */
	private function responseToStructure(Response $response) {

		//Раскодируем ответ в JSON
		try {
			$data = json_decode($response->getBody(), true);
			if( empty($data) && !is_array($data) ) {
				echo $response->getRequest()->getUrl();
				throw new \Exception('Не верный формат JSON ответа.');
			}


			//Разбираем ответ
			if($data['status'] == true) {
				if(empty($data['data']) ){
					$data['data'] = array();
				}
			} else {
//				if(empty($data['status']) ){
//					throw new \Exception('Сервер не вернул текст сообщения');
//				}
				throw new \Exception($data['message']);
			}

		} catch(\Exception $e) {
			$data = array(
				'status'=>false,
				'message'=>$e->getMessage()
			);
		}

		return $data;
	}


	/**
	 * Обновление новости
	 *
	 * @throws
	 * @param \NewsApi\Model\TranslateNews $model
	 * @return \NewsApi\Model\TranslateNews
	 */
	public function update(\NewsApi\Model\TranslateNews $model){
		$browser = $this->getBrowser();

		$request = $this->createRequest();
		//
		$m = array();
		$m['id'] = $model->getId();
		$m['title'] = $model->getTitle();
		$m['description'] = $model->getDescription();
		$m['html'] = $model->getHtml();
		$m['image'] = $model->getImage();
		$pubDate = $model->getPubDate();
		$m['pubDate'] = $pubDate->format('Y-m-d H:i:s');


		$request->setMethod('PUT');
		$request->setParams($m);

		//Выполняем запрос
		$response = $browser->exequteQuery($request);
		$httpStatus = $response->getStatus();


		if($httpStatus == 301) {
			$getUrl = $response->getHeader('Location');
			$newses = $this->getByUrl($getUrl);
			return $newses[0];
		}

		$structure = $this->responseToStructure($response);

		if(!$structure['status']) {
			throw new \Exception($structure['message']);
		}

		//todo:: Дописать обработчик ошибок

		throw new \Exception('Обновить неудалось');
	}

	/**
	 *
	 * @throws
	 * @param Filter $filter
	 * @return \NewsApi\Model\TranslateNews[]
	 */
	public function get(Filter $filter) {

		$baseApi = $this->getApi();

		//Формируем УРЛ для запроса.
		$url = $baseApi->constructUrl($this->host,
			$this->lang,
			$this->map,
			$this->iface,
			$this->cmd,
			$filter->getFilters());

		return $this->getByUrl($url);
	}


	/**
	 * GET запрос по указанному адресу.
	 *
	 * @param $url
	 * @return \NewsApi\Model\TranslateNews[]
	 * @throws \Exception
	 */
	private function getByUrl($url) {
		$browser = $this->getBrowser();
		$request = $this->createRequest();

		$request->setUrl($url);
		$request->setMethod('GET');

		//Выполняем запрос
		$response = $browser->exequteQuery($request);

		$structure = $this->responseToStructure($response);

		if(!$structure['status']) {
			throw new \Exception($structure['message']);
		}

		try{
			$ret = array();
			$items = $structure['data'];
			foreach($items as $i) {
				$model = new \NewsApi\Model\TranslateNews();
				//биндим модель
				$model->bindFromArray($i);
				$model->setLang($this->lang);

				$ret[] = $model;
			}

			return $ret;
		} catch (\Exception $e) {
			throw new \Exception('Ошибка обработки данных. '.$e->getMessage() );
		}



	}

} 