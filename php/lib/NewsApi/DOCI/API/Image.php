<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 11.06.14
 * Time: 17:47
 */

namespace NewsApi\DOCI\API;

use NewsApi\Filter;
use REST\AbstractAPI;
use REST\Request;
use REST\Response;

class Image extends  AbstractAPI {
	public $host = 'rga';
	public $lang = 'ru';
	public $map = '02.07.06';
	public $iface = 'restapi';
	public $cmd = 'image';

	/**
	 * Формирует болванку(стандартную заготовку) запроса
	 *
	 * @param Filter $filter
	 * @return  Request
	 */
	private function createRequest(Filter $filter=null) {
		$baseApi = $this->getApi();
		$request = new Request();

		//фильтр (параметры) для url
		$filters = array();
		if($filter) {
			$filters = $filter->getFilters();
		}

		//Формируем УРЛ для запроса.
		$url = $baseApi->constructUrl($this->host,
			$this->lang,
			$this->map,
			$this->iface,
			$this->cmd,
			$filters);

		$request->setUrl($url);
		//передаем параметры фильтрации
		return $request;
	}

	/**
	 * Преобразует ответ в структуру
	 * Ошибка:
	 *  {
	 *      status: false
	 *      message: 'some message'
	 *  }
	 *
	 * Все ок:
	 *  {
	 *      status: true
	 *      data: [...]
	 *  }
	 *
	 * @param Response $response
	 * @return array|mixed
	 */
	private function responseToStructure(Response $response) {

		//Раскодируем ответ в JSON
		try {
			$data = json_decode($response->getBody(), true);
			if( empty($data) && !is_array($data) ) {
				echo $response->getRequest()->getUrl();
				throw new \Exception('Не верный формат JSON ответа.');
			}

			//Разбираем ответ
			if($data['status'] == true) {
				if(empty($data['data']) ){
					$data['data'] = array();
				}
			} else {
				throw new \Exception($data['message']);
			}

		} catch(\Exception $e) {
			$data = array(
				'status'=>false,
				'message'=>$e->getMessage()
			);
		}

		return $data;
	}

	/**
	 * Создание новой новости
	 *
	 * @throws
	 * @param \NewsApi\Model\Image $model
	 * @return \NewsApi\Model\Image
	 */
	public function create(\NewsApi\Model\Image $model) {

		$browser = $this->getBrowser();
		$request = $this->createRequest();

		$filePath = $model->getFile();
		if(!$filePath) {
			throw new \Exception('Не указан файл предназначенный для загрузки на сервер.');
		}

		//if(!file_exists($filePath)) {
		//	throw new \Exception('Указанный файл не существует.');
		//}

		$request->setFiles(array(
			'image'=>'@'.$filePath
		));

		$request->setMethod('POST');


		//Выполняем запрос
		$response = $browser->exequteQuery($request);
		//$response = $browser->sendFile($request);
		//print_r($response);
		$httpStatus = $response->getStatus();

		if($httpStatus == 201) {
			$getUrl = $response->getHeader('Location');
			$newses = $this->getByUrl($getUrl);
			return $newses[0];
		}

		$struct =  $this->responseToStructure($response);
		if(!$struct['status']){
			throw new \Exception($struct['message']);
		}

		throw new \Exception('Загрузить файл не удалось');
	}


	/**
	 *
	 * @throws
	 * @param Filter $filter
	 * @return \NewsApi\Model\Image[]
	 */
	public function get(Filter $filter) {

		$baseApi = $this->getApi();

		//Формируем УРЛ для запроса.
		$url = $baseApi->constructUrl($this->host,
			$this->lang,
			$this->map,
			$this->iface,
			$this->cmd,
			$filter->getFilters());

		return $this->getByUrl($url);
	}


	/**
	 * GET запрос по указанному адресу.
	 *
	 * @param $url
	 * @return \NewsApi\Model\Image[]
	 * @throws \Exception
	 */
	private function getByUrl($url) {
		$browser = $this->getBrowser();
		$request = $this->createRequest();

		$request->setUrl($url);
		$request->setMethod('GET');

		//Выполняем запрос
		$response = $browser->exequteQuery($request);
		//print_r($response);
		$structure = $this->responseToStructure($response);

		if(!$structure['status']) {
			throw new \Exception($structure['message']);
		}

		try{
			$ret = array();
			$items = $structure['data'];
			foreach($items as $i) {
				$model = new \NewsApi\Model\Image();
				//биндим модель
				$model->bindFromArray($i);
				$ret[] = $model;
			}

			return $ret;
		} catch (\Exception $e) {
			throw new \Exception('Ошибка обработки данных. '.$e->getMessage() );
		}



	}


} 