<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 13:47
 */

namespace NewsApi\DOCI;
use NewsApi\DOCI\API\Config;
use NewsApi\DOCI\API\Image;
use NewsApi\DOCI\API\News;
use NewsApi\DOCI\API\TranslateNews;
use NewsApi\Site;
use REST\Browser;
use REST\Request;


/**
 * Class REST
 * @package NewsApi\DOCI
 */
final class REST {


	/**
	 * @var Site null
	 */
	private $site = null;


	/**
	 * @var null|\REST\Browser
	 */
	private $browser = null;

	/**
	 * Возвращает браузер HTTP запросов
	 *
	 * @return null|\REST\Browser
	 */
	public function getBrowser()
	{
		return $this->browser;
	}


	function __construct(Site $site) {

		$this->site = $site;
		$this->browser = new Browser();
	}


	/**
	 * Получение хоста сайта из строки подключения
	 *
	 * @return string
	 */
	function getHost() {
		//http://donoda.local/index.php?lang=ua&sec=02.03.20&iface=Admin&cmd=test
		$url = $this->site->getParam('url');
		return parse_url($url, PHP_URL_HOST );
	}


	/**
	 * Получение карты(раздела) сайта из строки подключения
	 *
	 * @return string
	 */
	function getMapId() {
		$url = $this->site->getParam('url');
		$query =  parse_url($url, PHP_URL_QUERY );
		parse_str($query, $out);
		return @$out['sec'];
	}


	/**
	 * Получение интерфейса из строки подключения
	 *
	 * @return string
	 */
	function getIface() {
		$url = $this->site->getParam('url');
		$query =  parse_url($url, PHP_URL_QUERY );
		parse_str($query, $out);
		return @$out['iface'];
	}

	/**
	 * Получение языка по умолчанию, из строки подключения
	 *
	 * @return string
	 */
	function getDefaultLang() {
		$url = $this->site->getParam('url');
		$query =  parse_url($url, PHP_URL_QUERY );
		parse_str($query, $out);
		return @$out['lang'];
	}



	protected $isLogin = false;

	/**
	 * Авторизация на сайте
	 */
	public function login(){

		//Если уже авторизован
		if($this->isLogin === true ){
			return;
		}

		$back_url = $this->site->getParam('url');
		$url = 'http://'.$this->getHost().'/?spec_cmd=login&backend='.urlencode($back_url);

		$login = $this->site->getParam('login');
		$password = $this->site->getParam('password');

		$browser = $this->getBrowser();
		$request = new Request();
		//$request->setAutoRedirect(true);
		$request->setMethod('POST');
		$request->setUrl($url);
		$request->setParams(array(
			'login'=>$login,
			'password'=>$password
		));

		$response = $browser->exequteQuery($request);
//		print_r($response);
//		echo "LOGIN ->".$response->getStatus()."\n";

		//Делательно дописать проверку на вход на сайт

		$this->isLogin = true;
	}


	/**
	 * Разавторизация
	 */
	public function unlogin(){

		//Если не авторизован
		if(!$this->isLogin){
			return;
		};

		$url = 'http://'.$this->getHost().'/?spec_cmd=login';

		$login = $this->site->getParam('login');
		$password = $this->site->getParam('password');

		$browser = $this->getBrowser();
		$request = new Request();
		$request->setUrl($url);
		$request->setMethod('POST');
		$request->setParams(array(
			'login'=>$login,
			'password'=>$password
		));

		$response = $browser->exequteQuery($request);
		$browser->cleanCookie();
		$this->isLogin = false;
	}


	/**
	 * Формирование строки адреса
	 *
	 * @param string $host хост сайта
	 * @param string $lang язык
	 * @param string $map раздел сайта
	 * @param string $iface интерфейс
	 * @param string $cmd команда
	 * @param array $param параметры
	 *
	 * @return mixed|string
	 */
	function constructUrl($host,$lang,$map,$iface,$cmd,$param=array()) {
		$url = 'http://#host#/index.php?lang=#lang#&sec=#map#&iface=#iface#&cmd=#cmd#&args=#args#';
		$url = str_replace('#host#',$host, $url);
		$url = str_replace('#lang#',$lang, $url);
		$url = str_replace('#map#',$map, $url);
		$url = str_replace('#iface#',$iface, $url);
		$url = str_replace('#cmd#',$cmd, $url);
		$url = str_replace('#args#', \TServ::EncodeArgs($param) , $url);
		return $url;
	}

	/**
	 * Возвращает апи для работы с новостями
	 *
	 *@return News
	 */
	public function getNewsApi() {
		$api = new News($this);
		//Устанавливаем настройки формирования url запроса
		$api->host = $this->getHost();//rda
		$api->map = $this->getMapId();//'02.07.06';
		$api->lang= $this->getDefaultLang();//ua

		return $api;
	}


	/**
	 * Возвращает апи для работы с переводами
	 *
	 * @param string $lang поддерживаемый язык
	 * @return TranslateNews
	 */
	public function getTranslateNewsApi($lang = null) {

		//если язык не указан, то язык по умолчанию
		if($lang === null ){
			$lang = $this->getDefaultLang();
		}

		$api = new TranslateNews($this);
		//Устанавливаем настройки формирования url запроса
		$api->host = $this->getHost();//rda
		$api->map = $this->getMapId();//'02.07.06';
		$api->lang= $lang;//ua

		return $api;

	}


	/**
	 * Возвращает API для работы с переводами
	 *
	 *
	 *@return Image
	 */
	public function getImageApi() {
		$api = new Image($this);
		//Устанавливаем настройки формирования url запроса
		$api->host = $this->getHost();
		$api->map = $this->getMapId();
		$api->lang= $this->getDefaultLang();
		return $api;
	}


	/**
	 * Возвращает API для работы с конфигурацией сайта
	 *
	 * @return Config
	 */
	public function getConfigApi(){
		$api = new Config($this);

		//Устанавливаем настройки формирования url запроса
		$api->host = $this->getHost();
		$api->map = $this->getMapId();
		$api->lang= $this->getDefaultLang();

		return $api;
	}



} 