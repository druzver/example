<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 11.06.14
 * Time: 11:44
 */

namespace NewsApi;


/**
 * Класс сайта.
 *
 * Задает общий интерфейс и функционал, используется для конфигурирования объекта API
 *
 * @package NewsApi
 */
class Site {

	/**
	 * Идентификатор
	 * @var integer
	 */
	protected $id;

	/**
	 * Название
	 * @var string
	 */
	protected $name = 'Uniknow site';

	/**
	 * Тип API
	 * @var string
	 */
	protected $type = 'doci';


	/**
	 * Настройки подключения
	 * @var array
	 */
	protected $params;


	/**
	 * Создает объект на основе массива данных
	 * @param array $data
	 * @return Site
	 */
	static public function createFromArray(array $data) {

		$s = new Site();

		if ( array_key_exists('id', $data) ) {
			$s->id = $data['id'];
		};

		if ( array_key_exists('name', $data) ) {
			$s->name = $data['name'];
		};

		if ( array_key_exists('type', $data) ) {
			$s->type = $data['type'];
		};

		if ( array_key_exists('params', $data) ) {
			$s->params = $data['params'];
		};

		return $s;
	}

	/**
	 * Кодирование параметров в строку
	 * @param array $ar
	 * @return string
	 */
	static public function encodeParams(array $ar){
		$ret = @ json_encode($ar);
		return $ret;
	}


	/**
	 * Раскодирование параметров из строки
	 * @param $str
	 * @return array
	 */
	static public function decodeParams($str) {
		$ret = array();

		if(is_string($str)){
			$ret = @json_decode($str,true);
			if(!$ret) {
				$ret = array();
			}
		}

		return $ret;
	}


	/**
	 * Возвращает идентификатор записи
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Возвращает название сайта
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Возвращает один параметр подключения
	 *
	 * @param string $key название параметра
	 * @return mixed
	 */
	public function getParam($key)
	{
		return $this->params[$key];
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

} 