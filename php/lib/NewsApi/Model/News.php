<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 14:01
 */

namespace NewsApi\Model;


/**
 * Класс модели новости
 *
 * @package NewsApi\Model
 */
class News {

	/**
	 * Идентификатор новости
	 * @var integer
	 */
	private $id;

	/**
	 * Заголовок новости
	 * @var string
	 */
	private $title;

	/**
	 * Аннотация к новости
	 * @var string
	 */
	private $description;

	/**
	 * Полная версия новости
	 * @var string
	 */
	private $html;

	/**
	 * Идентификатор изображения
	 *
	 * @var string
	 */
	private $image;

	/**
	 * Дата публикации
	 * @var \DateTime
	 */
	private $pubDate;

	/**
	 * @param string $description
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param string $html
	 */
	public function setHtml($html)
	{
		$this->html = $html;
	}

	/**
	 * @return string
	 */
	public function getHtml()
	{
		return $this->html;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $image
	 */
	public function setImage($image)
	{
		$this->image = $image;
	}

	/**
	 * @return string
	 */
	public function getImage()
	{
		return $this->image;
	}

	/**
	 * @param \DateTime $pubDate
	 */
	public function setPubDate($pubDate)
	{
		$this->pubDate = $pubDate;
	}

	/**
	 * @return \DateTime
	 */
	public function getPubDate()
	{
		return $this->pubDate;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}


	/**
	 * Заполняет свойства модели с помощью ассоциативного массива параметров
	 *
	 * @param array $data
	 */
	function bindFromArray($data) {

		$this->setId(@$data['id']);
		$this->setTitle(@$data['title']);
		$this->setDescription(@$data['description']);
		$this->setHtml(@$data['html']);
		$this->setImage(@$data['image']);

		if(@$data['pubDate']) {
			//Конвертируем строку
			$pubDate = new \DateTime($data['pubDate']);
			$this->setPubDate($pubDate);
		}
	}



} 