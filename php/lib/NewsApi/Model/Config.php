<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 14:03
 */

namespace NewsApi\Model;


/**
 * Class Config
 * @package NewsApi\Model
 */
class Config {

	/**
	 * @var array
	 */
	private $config = array();

	/**
	 * Возвращает массив конфигурации
	 *
	 * @return array
	 */
	public function getConfig()
	{
		return $this->config;
	}


	/**
	 * Заполнение модели данными с помощью массива
	 *
	 * Пример :
	 *  $data = array('id'=>'12345', 'url'=>'http;//someurl/getfile.php?id=12345');
	 *  $file =  new File();
	 *  $file->bindFromArray($data);
	 *
	 *
	 * @param array $arr массив параметров объекта
	 */
	function bindFromArray($arr){
		$this->config = $arr;
	}


} 