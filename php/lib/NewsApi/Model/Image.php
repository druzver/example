<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 14:03
 */

namespace NewsApi\Model;


/**
 * Модель изображения.
 *
 *
 *
 * @package NewsApi\Model
 */

class Image {

	/**
	 * Идентификатор изображения
	 * @var string
	 */
	private $id;

	/**
	 * Ссылка для скачивания файла
	 * @var string
	 */
	private $url;

	/**
	 * @param string $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param string $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}


	/**
	 *
	 * @var string
	 */
	private $file=null;


	/**
	 * Получить путь к файлу предназначенный для загрузки на сервер
	 *
	 * Файл есть только в не сохраненных моделях, предназначенных для загрузки
	 *
	 * @return string
	 */
	public function getFile()
	{
		return $this->file;
	}

	public function setFile($file)
	{
		$this->file = $file;
	}


	/**
	 * Заполнение модели данными с помощью массива
	 *
	 * Пример :
	 *  $data = array('id'=>'12345', 'url'=>'http;//someurl/getfile.php?id=12345');
	 *  $file =  new File();
	 *  $file->bindFromArray($data);
	 *
	 *
	 * @param array $arr массив параметров объекта
	 */
	function bindFromArray($arr){
		$this->id = $arr['id'];
		$this->url = $arr['url'];
	}


	/**
	 * Создание объекта Image для дальнейшей загрузки по API.
	 *
	 * создается пустой объект Image без id и url, но с заполненым свойством file
	 * При олучении файла, возвращается строка вида:
	 * /.../DataStore/<полный путь к файлу в хранилище>;type=<mime-тип файла>;name=<имя файла>
	 *
	 * @param \TCmdEngine $ENGINE Объект энджин
	 * @param string $fileId идентификатор ресурса из хранилища ресурсов (DataStore)
	 * @return Image
	 */
	static function createFromLocalResourceId($ENGINE, $fileId) {
		/** @var  \SysDataStore $ds **/
		$ds = $ENGINE->GetSysIfc('DataStore');

		//Получение информации о файле
		$info = $ds->GetInfo($fileId);

		//Формат инфо
		//    [id] => 9aa0ebcd69d37a4b32dc3bfbcc6f1356c8c91291646f62653a6e733a6d6574612f2220
		//    [part_id] => 001
		//    [section_id] => 3
		//    [section_path] => 02
		//    [original_name] => Осень.jpg
		//    [size] => 11310
		//    [mime_type] => image/jpeg
		//    [date_open] => 2012-11-12 14:38:08.128489
		//    [date_last] => 2014-06-03 14:38:12.105958
		//    [path] => /home/xuser/git/donoda/DataStore/001/02/9aa0ebcd69d37a4b32dc3bfbcc6f1356c8c91291646f62653a6e733a6d6574612f2220
		//    [add_mime_type] => Array
		//        (
		//            [name] => Изображение JPEG
		//            [small_icon] => /images/mimetypes/32/jpg.png
		//            [big_icon] => /images/mimetypes/128/jpg.png
		//        )

		$im = new Image();

		//Такой слохный путь используется курлом для передачи доп параметров
		$im->setFile($info['path'].';type='.$info['mime_type'].';name='.$info['original_name']);

		return $im;

	}

	/**
	 * Создание объекта Image для дальнейшей загрузки по API. из файла не в хранилище
	 *
	 * @param string $filePath путь к файлу
	 * @return Image
	 */
	static function createFromFile($filePath) {
		$im = new Image();
		$im->file = $filePath;
		return $im;
	}




}