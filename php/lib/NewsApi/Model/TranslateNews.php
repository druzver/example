<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 14:02
 */

namespace NewsApi\Model;


/**
 * Модель для работы с переводами новости.
 *
 * Наследуется от модели новости, поэтому содержит все ее свойисва и методы + методы по работе с языком
 *
 * @package NewsApi\Model
 */
class TranslateNews extends News {

	/**
	 * Язык перевода
	 * @var string
	 */
	private $lang;

	/**
	 * @param string $lang
	 */
	public function setLang($lang)
	{
		$this->lang = $lang;
	}

	/**
	 * @return string
	 */
	public function getLang()
	{
		return $this->lang;
	}





} 