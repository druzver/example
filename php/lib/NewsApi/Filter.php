<?php
/**
 * Created by PhpStorm.
 * User: xuser
 * Date: 05.06.14
 * Time: 14:30
 */

namespace NewsApi;


/**
 * Класс фильтра для GET запросов
 *
 *
 *
 * @package NewsApi
 */
class Filter {


	/**
	 * Создает новый экземпляр фильтра
	 *
	 * @param array $filters ассоциативный массив параметров фильтра
	 * @return Filter
	 */
	public static function create($filters=array()) {
		$filter = new Filter();
		$filter->filters = $filters;

		return $filter;
	}

	/**
	 * массив всех параметров фильтра
	 * @var array
	 */
	private $filters=array();

	/**
	 * Возвращает все фильтры в виде ассоциативного массива
	 * @return array
	 */
	public function getFilters()
	{
		return $this->filters;
	}


	/**
	 * Добавление параметра в фильтр
	 * Ф-ия является алайсом для метода set
	 *
	 * @param string $key ключ
	 * @param string $value значение
	 */
	function add($key, $value) {
		$this->set($key, $value);
	}

	/**
	 * Добавление параметра в фильтр
	 *
	 * @param string $key ключ
	 * @param string $value значение
	 */
	function set($key, $value) {
		$this->filters[$key] = $value;
	}

	/**
	 * Получение значения фильтра по ключу
	 *
	 * @param string $key ключ
	 * @return mixed
	 */
	function get($key) {
		if( $this->isExist($key)) {
			return $this->filters[$key];
		}

		return null;
	}

	/**
	 * Удаление значения фильтра по ключу
	 *
	 * @param string $key ключ
	 */
	function remove($key) {
		if($this->isExist($key)) {
			unset($this->filters[$key]);
		}
	}

	/**
	 * Проверка наличия ключа
	 *
	 * @param string $key ключ
	 * @return boolean
	 */
	function isExist($key) {
		$isExist = array_key_exists($key, $this->filters);
		return $isExist;
	}


	/**
	 * Отчистка фильтра
	 */
	function clear() {
		$this->filters = array();
	}




} 